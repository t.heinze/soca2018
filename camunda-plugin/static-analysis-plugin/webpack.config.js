const path = require('path');

module.exports = {
    module: {
      rules: [
        {test: /\.js$/, use: [{ loader: 'buble-loader' }]}
      ]
    },
    mode: 'none',
    entry: './client/client.js',
    output: {path: path.resolve(__dirname, 'client'), filename:'client-bundle.js'}
  };