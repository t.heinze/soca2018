from bottle import route, run, request, post
import os
import xml.etree.ElementTree as etree
from subprocess import Popen, PIPE
from xml2Ocaml import Translator, get_sequence_arrows, get_element_by_id_map, bpmn, translate_bpmn
from io import StringIO
import json
from multiprocessing import Process, Pipe


TRANSLATE_TIMEOUT_SEC = 2

def do_translate(tree, pipe):
    try:
        translated_diagram, translator = translate_bpmn(tree)
        pipe.send([translated_diagram, translator])
    except Exception as e:
        pipe.send([None, e])

@post('/hello')
def hello():
    print("*"*50 + " REQUEST")
    diagramXML = json.loads(request.body.read().decode("utf-8"))
    print(diagramXML[:100])
    tree = etree.parse(StringIO(diagramXML))
    
    pipe1, pipe2 = Pipe()
    proc = Process(target=do_translate, args=(tree, pipe2))
    proc.start()
    
    proc.join(TRANSLATE_TIMEOUT_SEC)

    if proc.is_alive():
        proc.terminate()
        proc.join()
        raise RuntimeError("Translation took too long. Possible invalid diagram.")
    
    translated_diagram, translator = pipe1.recv()

    if translated_diagram is None:
        raise translator

    with open ("../../coq/Process.ml", "w") as f:
        f.write(translated_diagram)
    
    start_analysesh = Popen("./analyse.sh", stdout=PIPE, cwd="../../coq")
    stdout, stderr = start_analysesh.communicate()
    print(stdout)
    print(stderr)

    analysis_result = stdout.decode("utf-8")
    print(analysis_result)
    analysis_result = json.loads(analysis_result)

    response = {"analysis_result" : analysis_result, "variables" : translator.variable_id }
    return response
    
    #analysisProcess = Popen('', stdin=PIPE)
    #analysisProcess.communicate(str(Translator(seq, elem_by_id).translate(start_arrow)))

 

run(host='0.0.0.0', port=1511, debug=True, reloader=True)
