'use strict';

module.exports = {
  name: 'Static Analysis Plugin',
  menu: './menu/menu.js',
  script: './client/client-bundle.js',
  style: './style/style.css'
};
