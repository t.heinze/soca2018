var registerBpmnJSPlugin = require('camunda-modeler-plugin-helpers').registerBpmnJSPlugin;
var analysisPlugin = require('./AnalysisPlugin');
var propertiesPlugin = require('./PropertiesPlugin');

registerBpmnJSPlugin(analysisPlugin);
registerBpmnJSPlugin(propertiesPlugin);
