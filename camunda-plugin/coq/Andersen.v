Require Import Tactics List.

Require Import Pointer AndersenModel AndersenSound AndersenIter.

Definition tainted a :=
  match a with
    | (_, true) => AllocSet.add AllocSet.empty a
    | _ => AllocSet.empty
  end.

Definition tSites s := fold_left AllocSet.union (map tainted s) AllocSet.empty.

Lemma in_fold1 : forall a ls s,
  AllocSet.In a s = true \/ (List.In a ls /\ taint a = true)
    -> AllocSet.In a (fold_left
      AllocSet.union (map tainted ls) s) = true.
  induction ls; simpl; intuition.

  generalize (IHls (AllocSet.union s (tainted a0))); intuition eauto.
  
  generalize (IHls (AllocSet.union s (tainted a0))); intuition.
  destruct H0; intuition; subst.
  apply H2.
  assert (AllocSet.In a0 (tainted a0) = true).
  destruct a0; unfold tainted; unfold taint in H1; subst; auto.
  eauto.
Qed.

Definition analysis : mustNotBeTainted_procedure.
  red; intros.
  destruct (fixed_point (abstractProcess process)) as [fp Hfp].

  caseEq (AllocSet.incl (tSites (AllocSet.elems (VarMap.sel (avars fp) v)))
    AllocSet.empty); intro Hincl.

  apply NotTainted.
  apply andersen_sound; intuition.
  match goal with
    | [ |- ?X = false ] => caseEq X
  end; trivial; intro Heq.

  assert (approx' abs fp); auto.
  destruct H1.
  wrong.

  caseEq (AllocSet.In site (tSites (AllocSet.elems (VarMap.sel (avars fp) v)))).
  assert (AllocSet.In site AllocSet.empty = true).  
  apply AllocSet.incl_In with (tSites (AllocSet.elems (VarMap.sel (avars fp) v))); auto.
  generalize (approxVars v); intro Hhelp.
  apply in_fold1; right; split; try apply AllocSet.elems_ok; eauto.
  rewrite AllocSet.In_empty in H1; discriminate.

  generalize (approxVars v); intro Hhelp.
  generalize (AllocSet.incl_In Hhelp H0); intro Hvars.
  assert (AllocSet.In site (tSites (AllocSet.elems (VarMap.sel (avars fp) v))) = true).
  apply in_fold1; right; split; try apply AllocSet.elems_ok; eauto.
  intros; congruence.

  apply MaybeTainted.
Qed.
