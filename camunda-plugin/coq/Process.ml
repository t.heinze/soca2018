let request = O
let score = S request
let rating = S score
let reply = S rating
let risk = O
let process = (Sequence ((Activity (Allocate request)), (Sequence ((Activity (Source score)), (Sequence ((Activity (Write (request, risk, score))), (Sequence ((Choice ((Activity (Read (rating, request, risk))), (Activity (Allocate rating)))), (Sequence ((Activity (Copy (reply, rating))), Empty))))))))))
let result_reply = (match analysis process reply with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")

let () = Printf.printf "{\"reply\":\"%s\"}\n" result_reply 