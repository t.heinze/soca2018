Require Import Arith List.

Require Import ListUtil Tactics Maps.
Require Import Pointer Machine AndersenModel AndersenSound.

Lemma incl_add : forall s a,
  AllocSet.incl s (AllocSet.add s a) = true.
  intros.
  generalize (AllocSet.In_add s a).
  generalize (AllocSet.incl_ok s (AllocSet.add s a)).
  intuition.
  apply H2; intros.
  generalize (H0 x); intuition.
Qed.

Lemma incl_add2 : forall s1 s2 a,
  AllocSet.incl s1 s2 = true
  -> AllocSet.incl (AllocSet.add s1 a) (AllocSet.add s2 a) = true.
  intros.
  generalize (AllocSet.incl_ok s1 s2).
  generalize (AllocSet.incl_ok (AllocSet.add s1 a) (AllocSet.add s2 a)).
  intuition.

  apply H3; intros.
  generalize (AllocSet.In_add s1 a x); intuition; subst.
  eauto.
  generalize (AllocSet.In_add s2 a x); intuition.
Qed.

Hint Resolve incl_add2.

Lemma incl_union2 : forall s1 s2 s3 s4,
  AllocSet.incl s1 s3 = true
  -> AllocSet.incl s2 s4 = true
  -> AllocSet.incl (AllocSet.union s1 s2)
  (AllocSet.union s3 s4) = true.
  intros.
  generalize (AllocSet.incl_ok s1 s3).
  generalize (AllocSet.incl_ok s2 s4).
  generalize (AllocSet.incl_ok (AllocSet.union s1 s2) (AllocSet.union s3 s4)).
  intuition.

  apply H5; intros.
  generalize (AllocSet.In_union H6); intuition eauto.
Qed.

Hint Resolve incl_union2.

Lemma incl_fold1 : forall (f : object -> AllocSet.set) x ls s,
  AllocSet.In x (fold_left
    (fun known site =>
      AllocSet.union known (f site))
    ls s) = true
  -> (AllocSet.In x s = true
    \/ exists y, In y ls /\ AllocSet.In x (f y) = true).
  induction ls; simpl; intuition.

  generalize (IHls (AllocSet.union s (f a))); intuition.
  destruct (AllocSet.In_union H0); intuition.
  right; eauto.

  destruct H0; intuition.
  right; eauto.
Qed.

Lemma incl_fold2 : forall (f : object -> AllocSet.set) x ls s,
  (AllocSet.In x s = true
    \/ exists y, In y ls /\ AllocSet.In x (f y) = true)
  -> AllocSet.In x (fold_left
    (fun known site =>
      AllocSet.union known (f site))
    ls s) = true.
  induction ls; simpl; intuition.

  firstorder.

  generalize (IHls (AllocSet.union s (f a))); intuition eauto.

  generalize (IHls (AllocSet.union s (f a))); intuition.
  destruct H0; intuition; subst.
  eauto.
  apply H2.
  eauto.
Qed.

Lemma incl_elems : forall t1 t2 y,
  AllocSet.incl t1 t2 = true
  -> In y (AllocSet.elems t1)
  -> In y (AllocSet.elems t2).
  intros.
  generalize (AllocSet.incl_ok t1 t2).
  generalize (AllocSet.elems_ok t1).
  generalize (AllocSet.elems_ok t2).
  intuition.

  generalize (H1 y).
  generalize (H2 y).
  intuition.
Qed.

Lemma incl_fold_read : forall (f1 f2 : object -> AllocSet.set) s1 s2 t1 t2,
  (forall a, AllocSet.incl (f1 a) (f2 a) = true)
  -> AllocSet.incl s1 s2 = true
  -> AllocSet.incl t1 t2 = true
  -> AllocSet.incl
  (fold_left
    (fun known site =>
      AllocSet.union known (f1 site))
    (AllocSet.elems t1) s1)
  (fold_left
    (fun known site =>
      AllocSet.union known (f2 site))
    (AllocSet.elems t2) s2) = true.
  intros.

  generalize (AllocSet.incl_ok
    (fold_left
      (fun (known : AllocSet.set) (site : object) =>
        AllocSet.union known (f1 site)) (AllocSet.elems t1) s1)
    (fold_left
      (fun (known : AllocSet.set) (site : object) =>
        AllocSet.union known (f2 site)) (AllocSet.elems t2) s2)); intuition.

  apply H4; intros.

  apply incl_fold2.
  destruct (incl_fold1 _ _ _ _ H2) as [Hcase | Hcase].

  eauto.

  destruct Hcase as [y [Helems Hin]].
  right; exists y; intuition.

  eapply incl_elems; eauto.

  eauto.
Qed.

Hint Resolve incl_fold_read.

Lemma incl_fold3' : forall a (f : allocation_site -> AllocSet.set) s ls h,
  AllocMap.sel h a = AllocSet.union (f a) s
  -> AllocMap.sel
  (fold_left (fun known site =>
    AllocMap.upd known (cell site) (AllocSet.union (f (cell site)) s))
  ls h) a = AllocSet.union (f a) s.
  induction ls; simpl; intuition.
  apply IHls.
  AllocMap_split.
Qed.

Fixpoint cells (ls : list object) : list nat :=
  match ls with
  | nil => nil
  | (a,b)::ls => a::(cells ls)
  end.

Lemma cell_helping_lemma' : forall ls a,
  In a ls -> In (cell a) (cells ls).
  induction ls; intros; intuition.
  inversion H.
  subst.
  assert (cells (a0 :: ls) = (cell a0) :: cells ls); destruct a0; auto.
  rewrite H0; apply in_eq.
  generalize (IHls a0 H0); intros.
  assert (cells (a :: ls) = (cell a) :: cells ls); destruct a; auto.
  rewrite H2; apply in_cons; trivial.
Qed.

Lemma cell_helping_lemma : forall ls1 ls2 a, 
  List.incl ls1 ls2
  -> In (cell a) (cells ls1)
  -> In (cell a) (cells ls2).
  induction ls1; intros.
  wrong; auto.
  destruct (eq_nat_dec (cell a0) ((cell a))).
  rewrite e; apply cell_helping_lemma'.
  generalize (H a); intros; apply H1.
  apply in_eq.
  apply (IHls1 ls2 a0).
  assert (incl ls1 (a :: ls1)); intuition.
  apply (incl_tran H1 H).
  assert (cells (a :: ls1) = (cell a) :: cells ls1).
  destruct a; auto.
  rewrite H1 in H0.
  inversion H0; trivial.
  wrong; intuition.
Qed.

Lemma incl_fold3 : forall f s a ls h,
  if In_eq_dec eq_nat_dec (cell a) (cells ls)
    then AllocMap.sel (fold_left (fun known site =>
      AllocMap.upd known (cell site) (AllocSet.union (f (cell site)) s))
    ls h) (cell a) = AllocSet.union (f (cell a)) s
    else AllocMap.sel (fold_left (fun known site =>
      AllocMap.upd known (cell site) (AllocSet.union (f (cell site)) s))
    ls h) (cell a) = AllocMap.sel h (cell a).
  induction ls; simpl; intuition.

  destruct (In_eq_dec eq_nat_dec (cell a) nil); trivial.
  inversion i.  

  destruct a0.

  destruct (In_eq_dec eq_nat_dec (cell a) (n :: cells ls)).
  destruct (eq_nat_dec (cell a) n).

  rewrite e; apply incl_fold3'; auto.
  apply AllocMap.sel_upd_eq.

  destruct (In_eq_dec eq_nat_dec (cell a) (cells ls)); intuition.

  wrong; inversion i; auto.

  destruct (In_eq_dec eq_nat_dec (cell a) (cells ls)); intuition.

  wrong; intuition.

  rewrite IHls.
  apply AllocMap.sel_upd_neq.
  simpl in n0.
  intuition.
Qed.

Lemma incl_fold_write' : forall a s1 s2 t1 t2 h1 h2,
  AllocSet.incl (AllocMap.sel h1 (cell a)) (AllocMap.sel h2 (cell a)) = true
  -> AllocSet.incl s1 s2 = true
  -> AllocSet.incl t1 t2 = true
  -> AllocSet.incl
  (AllocMap.sel (fold_left (fun known site =>
    AllocMap.upd known (cell site) (AllocSet.union (AllocMap.sel h1 (cell site)) s1))
  (AllocSet.elems t1) h1) (cell a))
  (AllocMap.sel (fold_left (fun known site =>
    AllocMap.upd known (cell site) (AllocSet.union (AllocMap.sel h2 (cell site)) s2))
  (AllocSet.elems t2) h2) (cell a)) = true.
  intros.

  generalize (incl_fold3 (AllocMap.sel h1) s1 a (AllocSet.elems t1) h1).
  generalize (incl_fold3 (AllocMap.sel h2) s2 a (AllocSet.elems t2) h2).
  destruct (In_eq_dec eq_nat_dec (cell a) (cells (AllocSet.elems t1)));
    destruct (In_eq_dec eq_nat_dec (cell a) (cells (AllocSet.elems t2)));
      intuition; rewrite H2; rewrite H3; auto.

  wrong.
  apply n.

  generalize (AllocSet.elems_ok t1 a).
  generalize (AllocSet.elems_ok t2 a).
  generalize (AllocSet.incl_ok t1 t2).
  intros.
  apply (cell_helping_lemma (AllocSet.elems t1) (AllocSet.elems t2)); trivial.
  unfold incl; intros.
  generalize (AllocSet.elems_ok t1 a0).
  generalize (AllocSet.elems_ok t2 a0).
  intuition.

  apply AllocSet.incl_trans with (AllocMap.sel h2 (cell a)); auto.
Qed.

Lemma incl_fold_write : forall a s1 s2 t1 t2 h1 h2,
  AllocSet.incl (AllocMap.sel h1 a) (AllocMap.sel h2 a) = true
  -> AllocSet.incl s1 s2 = true
  -> AllocSet.incl t1 t2 = true
  -> AllocSet.incl
  (AllocMap.sel (fold_left (fun known site =>
    AllocMap.upd known (cell site) (AllocSet.union (AllocMap.sel h1 (cell site)) s1))
  (AllocSet.elems t1) h1) a)
  (AllocMap.sel (fold_left (fun known site =>
    AllocMap.upd known (cell site) (AllocSet.union (AllocMap.sel h2 (cell site)) s2))
  (AllocSet.elems t2) h2) a) = true.
  intros.
  assert (a = cell (a,true)); auto.
  rewrite H2; rewrite H2 in H.
  eapply incl_fold_write'; auto.
Qed.

Hint Resolve incl_fold_write.

Definition ins_sites i :=
  match i with
    | AbsAllocate _ site => AllocSet.add (AllocSet.add AllocSet.empty (site, false)) (site, true)
    | AbsSource _ site => AllocSet.add (AllocSet.add AllocSet.empty (site, true)) (site, false)
    | _ => AllocSet.empty
  end.

Module VarSet : SET with Definition dom := var := ListSet(NatEq).

Definition ins_vars a :=
  match a with
    | AbsAllocate v _ => VarSet.add VarSet.empty v
    | AbsSource v _ => VarSet.add VarSet.empty v
    | AbsCopy v1 v2 => VarSet.add (VarSet.add VarSet.empty v1) v2
    | AbsSanitize v1 v2 => VarSet.add (VarSet.add VarSet.empty v1) v2
    | AbsRead v1 v2 fl => VarSet.add (VarSet.add VarSet.empty v1) v2
    | AbsWrite v1 fl v2 => VarSet.add (VarSet.add VarSet.empty v1) v2
  end.

Section prog.

  Variable process_as_list : list abstract_activity.

  Definition pSites := fold_left AllocSet.union (map ins_sites process_as_list).
  Definition pVars := fold_left VarSet.union (map ins_vars process_as_list).

End prog.

Section fixed_point.

  Variable process : compound_activity abstract_activity.
  Definition process_as_list := activities process.

  Definition procSites := pSites process_as_list AllocSet.empty.
  Definition procVars := pVars process_as_list VarSet.empty.

  Record approx' (abs1 abs2 : abstract_state) : Prop := {
    approxVars : forall v, AllocSet.incl
      (VarMap.sel (avars abs1) v) (VarMap.sel (avars abs2) v) = true;
    approxHeap : forall a, AllocSet.incl
      (AllocMap.sel (aheap abs1) a) (AllocMap.sel (aheap abs2) a) = true
  }.

  Hint Resolve AllocSet.incl_refl.

  Lemma increasing' : forall abs a,
    approx' abs (abstract_exec a abs).
    destruct a; intuition; simpl.
    
    VarMap_split; apply incl_add.

    VarMap_split; apply incl_add.

    VarMap_split.
    
    VarMap_split; apply incl_fold_union.

    VarMap_split; apply incl_fold_union.
    
    apply incl_write_bonanza.
  Qed.

  Lemma monotonic' : forall abs abs' a,
    approx' abs abs'
    -> approx' (abstract_exec a abs) (abstract_exec a abs').
    destruct a; intuition; simpl.

    destruct (eq_nat_dec v v0); subst.
    repeat rewrite VarMap.sel_upd_eq; eauto.
    repeat rewrite VarMap.sel_upd_neq; eauto.

    destruct (eq_nat_dec v v0); subst.
    repeat rewrite VarMap.sel_upd_eq; eauto.
    repeat rewrite VarMap.sel_upd_neq; eauto.

    destruct (eq_nat_dec v v1); subst.
    repeat rewrite VarMap.sel_upd_eq; eauto.
    repeat rewrite VarMap.sel_upd_neq; eauto.

    destruct (eq_nat_dec v v1); subst.
    repeat rewrite VarMap.sel_upd_eq; eauto.
    repeat rewrite VarMap.sel_upd_neq; eauto.

    destruct (eq_nat_dec v v1); subst.
    repeat rewrite VarMap.sel_upd_eq; eauto.
    repeat rewrite VarMap.sel_upd_neq; eauto.

    eauto.
  Qed.

  Lemma approx_refl' : forall s, approx' s s.
    intuition.
  Qed.

  Lemma approx_trans' : forall s1 s2 s3,
    approx' s1 s2 -> approx' s2 s3 -> approx' s1 s3.
    intuition.
    eapply AllocSet.incl_trans; eauto.
    eapply AllocSet.incl_trans; eauto.
  Qed.

  Lemma approx_init' : forall s, approx' abstract_initState s.
    unfold abstract_initState; intuition; simpl; intuition.
    rewrite VarMap.sel_init; apply AllocSet.incl_empty.
    rewrite AllocMap.sel_init; apply AllocSet.incl_empty.
  Qed.

  Record valid (abs : abstract_state) : Prop := {
    validVarsDom : forall v, VarSet.In v procVars = false
      -> AllocSet.incl (VarMap.sel (avars abs) v) AllocSet.empty = true;
    validHeapDom : forall a, AllocSet.In a procSites = false
      -> AllocSet.incl (AllocMap.sel (aheap abs) (cell a)) AllocSet.empty = true;
    validVars : forall v, AllocSet.incl (VarMap.sel (avars abs) v) procSites = true;
    validHeap : forall a, AllocSet.incl (AllocMap.sel (aheap abs) a) procSites = true
  }.

  Definition astate := sig valid.

  Definition approx (abs1 abs2 : astate) := approx' (proj1_sig abs1) (proj1_sig abs2).

  Record approxStrict (abs1 abs2 : astate) : Prop := {
    strictApprox : approx abs2 abs1;
    strictNe : (exists v, VarSet.In v procVars = true
      /\ AllocSet.incl (VarMap.sel (avars (proj1_sig abs1)) v)
      (VarMap.sel (avars (proj1_sig abs2)) v) = false)
    \/ (exists a, AllocSet.In a procSites = true
      /\ AllocSet.incl (AllocMap.sel (aheap (proj1_sig abs1)) (cell a))
      (AllocMap.sel (aheap (proj1_sig abs2)) (cell a)) = false)
  }.

  Definition aactivity := sig (fun ins => In ins process_as_list).

  Lemma fold_In_union : forall a ls s,
    AllocSet.In a s = true
    -> AllocSet.In a (fold_left AllocSet.union ls s) = true.
    induction ls; simpl; intuition eauto.
  Qed.

  Hint Resolve VarSet.incl_In VarSet.incl_union_left.

  Lemma fold_In_unionv : forall a ls s,
    VarSet.In a s = true
    -> VarSet.In a (fold_left VarSet.union ls s) = true.
    induction ls; simpl; intuition eauto.
  Qed.

  Lemma allocate_site : forall v a proc s,
    In (AbsAllocate v a) proc
      -> AllocSet.In (a,false) (pSites proc s) = true.
    unfold pSites.

    induction proc; simpl; intuition; subst.

    unfold pSites; simpl.
    apply fold_In_union.
    assert (AllocSet.In (a, false) (AllocSet.add AllocSet.empty (a, false)) = true); auto. 
    assert (AllocSet.In (a, false) (AllocSet.add (AllocSet.add AllocSet.empty (a, false)) (a, true)) = true).
    apply AllocSet.In_add; auto.
    eauto.
  Qed.

  Lemma source_site : forall v a proc s,
    In (AbsSource v a) proc
      -> AllocSet.In (a,true) (pSites proc s) = true.
    unfold pSites.

    induction proc; simpl; intuition; subst.

    unfold pSites; simpl.
    assert (AllocSet.In (a, true) (AllocSet.add AllocSet.empty (a, true)) = true); auto. 
    assert (AllocSet.In (a, true) (AllocSet.add (AllocSet.add AllocSet.empty (a, true)) (a, false)) = true).
    apply AllocSet.In_add; auto.
    apply fold_In_union.
    eauto.
  Qed.

  Lemma incl_add3 : forall x s1 s2,
    AllocSet.In x s2 = true
    -> AllocSet.incl s1 s2 = true
    -> AllocSet.incl (AllocSet.add s1 x) s2 = true.
    intros.
    generalize (AllocSet.incl_ok (AllocSet.add s1 x) s2); intuition.
    apply H3; intros.
    generalize (AllocSet.In_add s1 x x0);
      generalize (AllocSet.incl_ok s1 s2);
	intuition; subst; intuition.
  Qed.

  Lemma incl_union : forall s1 s2 s3,
    AllocSet.incl s1 s3 = true
    -> AllocSet.incl s2 s3 = true
    -> AllocSet.incl (AllocSet.union s1 s2) s3 = true.
    intros.
    generalize (AllocSet.incl_ok (AllocSet.union s1 s2) s3); intuition.
    apply H3; intros.
    generalize (AllocSet.incl_ok s1 s3);
      generalize (AllocSet.incl_ok s2 s3);
	generalize (AllocSet.In_union H1);
	  intuition.
  Qed.

  Lemma incl_valid_read : forall (f : object -> AllocSet.set) s2 ls s1,
    AllocSet.incl s1 s2 = true
    -> (forall site, AllocSet.incl (f site) s2 = true)
    -> AllocSet.incl
    (fold_left
      (fun known site => AllocSet.union known (f site))
      ls s1) s2 = true.
    induction ls; simpl; intuition.
    apply IHls; auto.
    apply incl_union; auto.
  Qed.

  Lemma incl_valid_write : forall a s1 s2 f ls h,
    AllocSet.incl s1 s2 = true
    -> (forall a, AllocSet.incl (f a) s2 = true)
    -> (forall a, AllocSet.incl (AllocMap.sel h a) s2 = true)
    -> AllocSet.incl
    (AllocMap.sel
      (fold_left
	(fun known site => AllocMap.upd known (cell site)
          (AllocSet.union (f site) s1))
	ls h) a)
    s2 = true.
    induction ls; simpl; intuition.

    apply IHls; intuition.
    AllocMap_split.
    apply incl_union; auto.
  Qed.
  
  Hint Resolve VarSet.In_add_eq VarSet.In_add_neq VarSet.incl_union_right.

  Lemma allocate_var : forall v a proc s,
    In (AbsAllocate v a) proc
      -> VarSet.In v (pVars proc s) = true.
    unfold pVars.

    induction proc; simpl; intuition; subst.

    unfold pVars; simpl.
    apply fold_In_unionv; eauto.
  Qed.

  Lemma source_var : forall v a proc s,
    In (AbsSource v a) proc
      -> VarSet.In v (pVars proc s) = true.
    unfold pVars.

    induction proc; simpl; intuition; subst.

    unfold pVars; simpl.
    apply fold_In_unionv; eauto.
  Qed.

  Lemma copy_var : forall v a proc s,
    In (AbsCopy v a) proc
      -> VarSet.In v (pVars proc s) = true.
    unfold pVars.

    induction proc; simpl; intuition; subst.

    unfold pVars; simpl.
    apply fold_In_unionv.
    apply VarSet.incl_In with (VarSet.add (VarSet.add VarSet.empty v) a).
    eauto.
    destruct (eq_nat_dec v a); subst; eauto.
  Qed.

  Lemma read_var : forall v a fl proc s,
    In (AbsRead v a fl) proc
      -> VarSet.In v (pVars proc s) = true.
    unfold pVars.

    induction proc; simpl; intuition; subst.

    unfold pVars; simpl.
    apply fold_In_unionv.
    apply VarSet.incl_In with (VarSet.add (VarSet.add VarSet.empty v) a).
    eauto.
    destruct (eq_nat_dec v a); subst; eauto.
  Qed.

  Lemma write_var : forall v a fl proc s,
    In (AbsWrite v fl a) proc
      -> VarSet.In v (pVars proc s) = true.
    unfold pVars.

    induction proc; simpl; intuition; subst.

    unfold pVars; simpl.
    apply fold_In_unionv.
    apply VarSet.incl_In with (VarSet.add (VarSet.add VarSet.empty v) a).
    eauto.
    destruct (eq_nat_dec v a); subst; eauto.
  Qed.

  Lemma sel_write_valid : forall f s a ls h,
    AllocMap.sel
    (fold_left (fun known site => AllocMap.upd known (cell site)
      (AllocSet.union (f (cell site)) s))
    ls h) (cell a)
    = if In_eq_dec eq_nat_dec (cell a) (cells ls)
      then AllocSet.union (f (cell a)) s
      else AllocMap.sel h (cell a).
    induction ls; simpl; intuition;
      match goal with
	| [ |- context[if ?X then _ else _] ] => destruct X
      end; simpl in *; intuition; subst; intuition.

    destruct a0; intuition.

    destruct (eq_nat_dec (cell a) n).
    rewrite e; apply incl_fold3'; auto.
    apply AllocMap.sel_upd_eq.

    destruct (In_eq_dec eq_nat_dec (cell a) (cells ls)); intuition.

    wrong.
    inversion i; auto.

    destruct a0; intuition.
    
    destruct (In_eq_dec eq_nat_dec (cell a) (cells ls)); intuition.
    generalize (in_cons n0 _ _ i); intuition.

    rewrite IHls.
    rewrite AllocMap.sel_upd_neq; auto.
    destruct (eq_nat_dec (cell a) n0); auto.
    rewrite e in n.
    intuition.
  Qed.

  Lemma aexec'_helping_lemma: forall a ls,
    In (cell a) (cells ls)
    -> In ((cell a), true) ls \/ In ((cell a), false) ls.
    induction ls; intros.
    wrong; intuition.

    assert (cells (a0 :: ls) = (cell a0) :: cells ls); destruct a0; auto.
    rewrite H0 in H.
    inversion H.

    rewrite <- H1.
    destruct b.
    left; rewrite H1 in H; intuition.
    right; rewrite H1 in H; intuition.

    destruct (IHls H1).
    left; intuition.
    right; intuition.
  Qed.

  Lemma proc_sites_lemma_true': forall a i,
    AllocSet.In (cell a, true) (ins_sites i) = true -> AllocSet.In a (ins_sites i) = true.
    unfold ins_sites; intros. 
    destruct a; destruct b; auto.
    unfold cell in H.
    induction i.

    destruct (eq_nat_dec a n).
    subst.
    assert (AllocSet.In (n, false) (AllocSet.add AllocSet.empty (n, false)) = true); auto.
    apply AllocSet.In_add; auto.

    wrong.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, true) = cell (n, true)).
    rewrite H; auto.
    unfold cell in H0; auto.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, false) = cell (n, true)).
    rewrite H; auto.
    unfold cell in H0; auto.
    assert (AllocSet.In (n, true) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.

    destruct (eq_nat_dec a n).
    subst.
    assert (AllocSet.In (n, true) (AllocSet.add AllocSet.empty (n, true)) = true); auto.

    wrong.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, false) = cell (n, true)).
    rewrite H; auto.
    unfold cell in H0; auto.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, true) = cell (n, true)).
    rewrite H; auto.
    unfold cell in H0; auto.
    assert (AllocSet.In (n, true) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.

    assert (AllocSet.In (n, true) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    assert (AllocSet.In (n, true) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    assert (AllocSet.In (n, true) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    assert (AllocSet.In (n, true) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
  Qed.

  Lemma proc_sites_lemma_false': forall a i,
    AllocSet.In (cell a, false) (ins_sites i) = true -> AllocSet.In a (ins_sites i) = true.
    unfold ins_sites; intros. 
    destruct a; destruct b; auto.
    unfold cell in H.
    induction i.

    destruct (eq_nat_dec a n).
    subst.
    assert (AllocSet.In (n, true) (AllocSet.add AllocSet.empty (n, true)) = true); auto.
    apply AllocSet.In_add; auto.

    wrong.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, true) = cell (n, false)).
    rewrite H; auto.
    unfold cell in H0; auto.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, false) = cell (n, false)).
    rewrite H; auto.
    unfold cell in H0; auto.
    assert (AllocSet.In (n, false) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.

    destruct (eq_nat_dec a n).
    subst.
    assert (AllocSet.In (n, false) (AllocSet.add AllocSet.empty (n, false)) = true); auto.
    apply AllocSet.In_add; auto.

    wrong.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, false) = cell (n, false)).
    rewrite H; auto.
    unfold cell in H0; auto.
    apply AllocSet.In_add in H.
    destruct H.
    assert (cell (a, true) = cell (n, false)).
    rewrite H; auto.
    unfold cell in H0; auto.
    assert (AllocSet.In (n, false) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.

    assert (AllocSet.In (n, false) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    assert (AllocSet.In (n, false) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    assert (AllocSet.In (n, false) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    assert (AllocSet.In (n, false) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
  Qed.

  Lemma incl_fold_help1 : forall (f : AllocSet.set -> AllocSet.set) x ls s,
    AllocSet.In x (fold_left
      (fun known site =>
        AllocSet.union known (f site))
      ls s) = true
    -> (AllocSet.In x s = true
      \/ exists y, In y ls /\ AllocSet.In x (f y) = true).
    induction ls; simpl; intuition.

    generalize (IHls (AllocSet.union s (f a))); intuition.
    destruct (AllocSet.In_union H0); intuition.
    right; eauto.

    destruct H0; intuition.
    right; eauto.
  Qed.

  Lemma incl_fold_help2 : forall (f : AllocSet.set -> AllocSet.set) x ls s,
    (AllocSet.In x s = true
      \/ exists y, In y ls /\ AllocSet.In x (f y) = true)
    -> AllocSet.In x (fold_left
      (fun known site =>
        AllocSet.union known (f site))
      ls s) = true.
    induction ls; simpl; intuition.

    firstorder.

    generalize (IHls (AllocSet.union s (f a))); intuition eauto.

    generalize (IHls (AllocSet.union s (f a))); intuition.
    destruct H0; intuition; subst.
    eauto.
    apply H2.
    eauto.
  Qed.

  Lemma proc_sites_lemma: forall a b,
    AllocSet.In (cell a, b) procSites = true -> AllocSet.In a procSites = true.
    unfold procSites.
    unfold pSites.
    intros.
    induction process_as_list.
    wrong.   

    generalize (incl_fold_help1 _ _ _ _ H); intros.
    destruct H0.
    wrong.
    assert (AllocSet.In (cell a, b) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    destruct H0 as [y [Hin1 Hin2]].
    intuition.

    inversion H.
    rewrite H1.

    assert (a0 :: l = (a0 :: nil) ++ l); auto.
    assert (map ins_sites (a0 :: l) = (map ins_sites (a0 :: nil)) ++ (map ins_sites l)); auto.
    assert ((fold_left AllocSet.union (map ins_sites (a0 :: l)) AllocSet.empty)
      = (fold_left AllocSet.union (map ins_sites l) ((fold_left AllocSet.union (map ins_sites (a0 :: nil)) AllocSet.empty)))).
    rewrite H2. apply fold_left_app.

    caseEq (AllocSet.In (cell a, b)
        (fold_left AllocSet.union (map ins_sites l) AllocSet.empty)); intros.

    apply IHl in H4.  
    apply incl_fold_help2.
    right.

    generalize (incl_fold_help1 _ _ _ _ H4); intros.
    destruct H5.
    wrong.
    assert (AllocSet.In a AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    destruct H5 as [y [Hin1 Hin2]].
    rewrite H2.
    exists y.
    intuition.

    apply incl_fold_help2.
    right.

    rewrite H3 in H.
    generalize (incl_fold_help1 _ _ _ _ H); intros.
    destruct H5.
    generalize (incl_fold_help1 _ _ _ _ H5); intros.
    destruct H6.
    wrong.
    assert (AllocSet.In (cell a, b) AllocSet.empty = false); try apply AllocSet.In_empty.
    congruence.
    destruct H6 as [y [Hin1 Hin2]].
    exists y.
    assert (map ins_sites (a0 :: nil) = (ins_sites a0) :: nil); auto.
    rewrite H6 in Hin1.
    inversion Hin1.  
    split.
    rewrite H2; intuition.
    subst; destruct a.
    unfold cell in Hin2.
    destruct b.
    apply proc_sites_lemma_true'; unfold cell; auto.
    apply proc_sites_lemma_false'; unfold cell; auto.
    wrong; intuition.

    wrong.   
    destruct H5 as [y [Hin1 Hin2]].
    generalize (incl_fold_help2 (fun x => x) (cell a,b) (map ins_sites l) AllocSet.empty); intros.
    assert (AllocSet.In (cell a, b)
       (fold_left (fun known site : AllocSet.set => AllocSet.union known site)
          (map ins_sites l) AllocSet.empty) = true).
    apply H5.
    right.
    exists y; intuition.
    assert ((fold_left AllocSet.union (map ins_sites l) AllocSet.empty)
      = (fold_left (fun known site : AllocSet.set => AllocSet.union known site)
          (map ins_sites l) AllocSet.empty)). intuition.
    congruence.
  Qed.

  Lemma sanitize_var : forall v a proc s,
    In (AbsSanitize v a) proc
      -> VarSet.In v (pVars proc s) = true.
    unfold pVars.

    induction proc; simpl; intuition; subst.

    unfold pVars; simpl.
    apply fold_In_unionv.
    apply VarSet.incl_In with (VarSet.add (VarSet.add VarSet.empty v) a).
    eauto.
    destruct (eq_nat_dec v a); subst; eauto.
  Qed.

  Lemma incl_valid_sanitize : forall (f : object -> AllocSet.set) s2 ls s1,
    AllocSet.incl s1 s2 = true
    -> incl ls (AllocSet.elems s2)
    -> (forall site, In site ls -> AllocSet.incl (f site) s2 = true)
    -> AllocSet.incl
    (fold_left
      (fun known site => AllocSet.union known (f site))
      ls s1) s2 = true.
    induction ls; simpl; intuition.
    apply IHls; auto.
    apply incl_union; auto.
    unfold incl in H0; unfold incl; intuition.
  Qed.

  Lemma aexec' : forall a abs, In a process_as_list -> valid abs -> valid (abstract_exec a abs).
    intros a abs Hins Habs.
    destruct Habs.
    destruct a; split; simpl; intuition.

    VarMap_split.
    wrong.
    unfold procVars in H; rewrite allocate_var with v0 a process_as_list VarSet.empty in H;
      auto; discriminate.

    VarMap_split.
    apply incl_add3; auto.
    unfold procSites; eapply allocate_site; eauto.

    VarMap_split.
    wrong.
    unfold procVars in H; rewrite source_var with v0 a process_as_list VarSet.empty in H;
      auto; discriminate.

    VarMap_split.
    apply incl_add3; auto.
    unfold procSites; eapply source_site; eauto.

    VarMap_split.
    wrong.
    unfold procVars in H; rewrite copy_var with v1 v0 process_as_list VarSet.empty in H;
      auto; discriminate.

    VarMap_split.
    apply incl_union; auto.

    VarMap_split.
    wrong.
    unfold procVars in H; rewrite sanitize_var with v1 v0 process_as_list VarSet.empty in H;
      auto; discriminate.

    VarMap_split.
    apply incl_valid_sanitize; auto.

    unfold incl; intuition.
    generalize (AllocSet.elems_ok procSites a); intros.
    generalize (AllocSet.elems_ok (VarMap.sel (avars abs) v0) a); intros.
    apply H0; apply H1 in H.
    generalize (validVars0 v0); intros.
    generalize (AllocSet.incl_ok (VarMap.sel (avars abs) v0) procSites); intuition.

    intuition.
    generalize (AllocSet.elems_ok (VarMap.sel (avars abs) v0) site); intros.
    apply H0 in H.
    generalize (validVars0 v0); intros.
    generalize (AllocSet.incl_ok (VarMap.sel (avars abs) v0) procSites); intuition.
    generalize (H4 site); intuition.
    assert (AllocSet.In ((cell site), false) procSites = true).
    destruct site; apply proc_sites_lemma with (b:=b); intuition.
    apply (AllocSet.incl_ok (AllocSet.add AllocSet.empty (cell site, false)) procSites); intuition.
    destruct (eq_natboolpair_dec x ((cell site), false)).
    rewrite e; auto.
    apply AllocSet.In_add in H7.
    destruct H7.
    congruence.
    generalize (AllocSet.In_empty x).
    congruence.

    VarMap_split.
    unfold procVars in H; rewrite read_var with v1 v0 f process_as_list VarSet.empty in H;
      auto; discriminate.

    VarMap_split.
    apply incl_valid_read; auto.

    rewrite sel_write_valid.
    match goal with
      | [ |- context[if ?X then _ else _] ] => destruct X
    end; auto.
    caseEq (VarSet.In v procVars); intro Hin.
    assert (false = true); [idtac | discriminate].
    rewrite <- H.

    generalize (AllocSet.elems_ok (VarMap.sel (avars abs) v)); intuition.

    apply aexec'_helping_lemma in i.

    destruct i.
    generalize (H0 ((cell a), true)); intuition.  
    apply proc_sites_lemma with (b:=true).
    apply AllocSet.incl_In with (VarMap.sel (avars abs) v); auto.
    generalize (H0 ((cell a), false)); intuition.  
    apply proc_sites_lemma with (b:=false).
    apply AllocSet.incl_In with (VarMap.sel (avars abs) v); auto.

    unfold procVars in Hin; rewrite write_var with v v0 f process_as_list VarSet.empty in Hin;
      auto; discriminate.
    
    apply incl_valid_write; auto.
  Qed.

  Definition aexec : aactivity -> astate -> astate.
    intros a abs.
    destruct a as [a Hins].
    destruct abs as [abs Habs].
    exists (abstract_exec a abs).
    apply aexec'; trivial.
  Defined.

  Definition ainitState : astate.
    exists abstract_initState.
    unfold abstract_initState.
    split; simpl; intuition.

    rewrite VarMap.sel_init; apply AllocSet.incl_empty.
    rewrite AllocMap.sel_init; apply AllocSet.incl_empty.
    rewrite VarMap.sel_init; apply AllocSet.incl_empty.
    rewrite AllocMap.sel_init; apply AllocSet.incl_empty.
  Defined.

  Theorem increasing : forall abs a,
    approx abs (aexec a abs).
    destruct abs as [abs Habs].
    destruct a as [a Hins].
    unfold approx.
    simpl.
    apply increasing'.
  Qed.

  Hint Resolve increasing.

  Theorem monotonic : forall abs abs' a,
    approx abs abs'
    -> approx (aexec a abs) (aexec a abs').
    destruct abs as [abs Habs].
    destruct abs' as [abs' Habs'].
    destruct a as [a Hins].
    unfold approx.
    simpl.
    apply monotonic'.
  Qed.

  Hint Resolve monotonic.

  Theorem approx_refl : forall s, approx s s.
    destruct s as [s Hs].
    unfold approx.
    simpl.
    apply approx_refl'.
  Qed.

  Hint Resolve approx_refl.

  Theorem approx_trans : forall s1 s2 s3,
    approx s1 s2 -> approx s2 s3 -> approx s1 s3.
    destruct s1.
    destruct s2.
    destruct s3.
    unfold approx.
    simpl.
    apply approx_trans'.
  Qed.

  Hint Resolve approx_trans.

  Theorem approx_init : forall s, approx ainitState s.
    destruct s.
    unfold approx, ainitState.
    simpl.
    apply approx_init'.
  Qed.

  Hint Resolve approx_init.

  Hint Resolve AllocSet.incl_empty.

  Theorem approxStrict_approx : forall s1 s2,
    approx s2 s1
    -> ~approxStrict s1 s2
    -> approx s1 s2.
    destruct s1.
    destruct s2.
    intuition.
    destruct H.
    split; simpl in *; intuition.

    caseEq (AllocSet.incl (VarMap.sel (avars x) v1) (VarMap.sel (avars x0) v1)); intro Heq; trivial.
    wrong.
    apply H0.
    intuition.
    unfold approx; simpl; intuition.
    left.
    exists v1.
    intuition.

    inversion v.
    caseEq (VarSet.In v1 procVars); intro Heq'; trivial.
    rewrite <- Heq.
    apply AllocSet.incl_trans with AllocSet.empty; eauto.
    
    caseEq (AllocSet.incl (AllocMap.sel (aheap x) a) (AllocMap.sel (aheap x0) a));
    intro Heq; trivial.
    wrong.
    apply H0.
    intuition.
    unfold approx; simpl; intuition.
    right.
    exists (a,true).
    intuition.

    inversion v.
    caseEq (AllocSet.In (a,true) procSites); intro Heq'; trivial.
    rewrite <- Heq.
    apply AllocSet.incl_trans with AllocSet.empty; eauto.
    apply (validHeapDom0 (a,true)); auto.
  Qed.

  Hint Resolve approxStrict_approx.

  Theorem approxStrict_trans : forall s1 s2 s3,
    approx s2 s1 -> approxStrict s2 s3 -> approxStrict s1 s3.
    destruct s1.
    destruct s2.
    destruct s3.
    unfold approx; simpl.
    intuition; unfold approx in *; simpl in *; intuition.

    eapply AllocSet.incl_trans; eauto.

    eapply AllocSet.incl_trans; eauto.

    left.
    destruct H as [v' [Hin Hincl]].
    exists v'; intuition.
    caseEq (AllocSet.incl (VarMap.sel (avars x) v') (VarMap.sel (avars x1) v')); trivial; intro Heq.
    rewrite <- Hincl.
    symmetry.
    eapply AllocSet.incl_trans; eauto.

    eapply AllocSet.incl_trans; eauto.

    eapply AllocSet.incl_trans; eauto.

    right.
    destruct H as [a [Hin Hincl]].
    exists a. intuition.
    caseEq (AllocSet.incl (AllocMap.sel (aheap x) (cell a)) (AllocMap.sel (aheap x1) (cell a))); trivial; intro Heq.
    rewrite <- Hincl.
    symmetry.
    eapply AllocSet.incl_trans; eauto.
  Qed.

  Hint Resolve approxStrict_trans.

  Definition approx_dec : forall abs1 abs2, {approx abs1 abs2} + {~approx abs1 abs2}.
    destruct abs1 as [abs1 Habs1].
    destruct abs2 as [abs2 Habs2].
      
    assert (checkVar : forall v,
      {AllocSet.incl
	(VarMap.sel (avars abs1) v)
	(VarMap.sel (avars abs2) v) = true}
      + {~AllocSet.incl
	(VarMap.sel (avars abs1) v)
	(VarMap.sel (avars abs2) v) = true}).
    intro.
    destruct (AllocSet.incl (VarMap.sel (avars abs1) v)
      (VarMap.sel (avars abs2) v)); auto.

    assert (checkHeap : forall v,
      {AllocSet.incl
	(AllocMap.sel (aheap abs1) (cell v))
	(AllocMap.sel (aheap abs2) (cell v)) = true}
      + {~AllocSet.incl
	(AllocMap.sel (aheap abs1) (cell v))
	(AllocMap.sel (aheap abs2) (cell v)) = true}).
    intro.
    destruct (AllocSet.incl (AllocMap.sel (aheap abs1) (cell v))
      (AllocMap.sel (aheap abs2) (cell v))); auto.

    refine (match AllS_dec (fun v => AllocSet.incl
      (VarMap.sel (avars abs1) v)
      (VarMap.sel (avars abs2) v) = true)
    checkVar (VarSet.elems procVars) with
	      | left Hvars =>
		match AllS_dec (fun v => AllocSet.incl
		  (AllocMap.sel (aheap abs1) (cell v))
		  (AllocMap.sel (aheap abs2) (cell v)) = true)
		checkHeap (AllocSet.elems procSites) with
		  | left Hheap => left _ _
		  | right Hheap => right _ _
		end
	      | right Hvars => right _ _
	    end); intuition.

    unfold approx in *; simpl in *; intuition.

    caseEq (VarSet.In v procVars); intro Heq.
    pattern v; eapply AllS_In; eauto.
    generalize (VarSet.elems_ok procVars); intuition.
    generalize (H v); intuition.
    
    inversion Habs1.
    apply AllocSet.incl_trans with AllocSet.empty; eauto.

    caseEq (AllocSet.In (a,true) procSites); intro Heq.
    assert (cell (a,true) = a); auto.
    rewrite <- H.
    pattern (a,true); eapply AllS_In; eauto.
    generalize (AllocSet.elems_ok procSites); intuition.
    generalize (H0 (a,true)); intuition.

    inversion Habs1.
    assert (cell (a,true) = a); auto.
    rewrite <- H.
    apply AllocSet.incl_trans with AllocSet.empty; eauto.

    unfold approx in H. simpl in H. intuition.
    apply Hheap.
    apply AllS_deduce. auto.

    unfold approx in H; simpl in H; intuition.
    apply Hvars.
    apply AllS_deduce; auto.
  Qed.

  Definition approxStrict_dec : forall abs1 abs2, {approxStrict abs1 abs2} + {~approxStrict abs1 abs2}.
    destruct abs1 as [abs1 Habs1].
    destruct abs2 as [abs2 Habs2].

    assert (checkVar : forall v,
      {AllocSet.incl (VarMap.sel (avars abs1) v)
	(VarMap.sel (avars abs2) v) = true}
      + {~AllocSet.incl (VarMap.sel (avars abs1) v)
	(VarMap.sel (avars abs2) v) = true}).
    intros.
    match goal with
      [ |- {?X = true} + {_} ] => destruct X
    end; intuition.

    assert (checkHeap : forall v,
      {AllocSet.incl (AllocMap.sel (aheap abs1) (cell v))
	(AllocMap.sel (aheap abs2) (cell v)) = true}
      + {~AllocSet.incl (AllocMap.sel (aheap abs1) (cell v))
	(AllocMap.sel (aheap abs2) (cell v)) = true}).
    intros.
    match goal with
      [ |- {?X = true} + {_} ] => destruct X
    end; intuition.

    refine (match approx_dec (exist _ abs2 Habs2) (exist _ abs1 Habs1) with
	      | left Happrox =>
		match AllS_dec_some (fun v => AllocSet.incl (VarMap.sel (avars abs1) v)
		  (VarMap.sel (avars abs2) v) = true)
		checkVar (VarSet.elems procVars) with
		  | left Hvars =>
		    match AllS_dec_some (fun v => AllocSet.incl (AllocMap.sel (aheap abs1) (cell v))
		      (AllocMap.sel (aheap abs2) (cell v)) = true)
		    checkHeap (AllocSet.elems procSites) with
		      | left Hheap => right _ _
		      | right Hheap => left _ _
		    end
		  | right Hvars => left _ _
		end
	      | right Happrox => right _ _
	    end); intuition; try (injection Heq; intros; subst; intuition).

    destruct H as [v [Hin Hincl]].
    simpl in *; idtac.
    rewrite (AllS_In Hvars) in Hincl; try discriminate.
    generalize (VarSet.elems_ok procVars); intuition.
    generalize (H v); intuition.

    destruct H as [a [Hin Hincl]].
    simpl in *; idtac.
    rewrite (AllS_In Hheap) in Hincl; try discriminate.
    generalize (AllocSet.elems_ok procSites); intuition.
    generalize (H a); intuition.
    
    right.
    destruct (AllS_not (f := fun v : object =>
      AllocSet.incl (AllocMap.sel (aheap abs1) (cell v))
      (AllocMap.sel (aheap abs2) (cell v)) = true) checkHeap H); intuition.
    destruct H1 as [x [Hin Hincl]].
    exists x; simpl; intuition.
    generalize (AllocSet.elems_ok procSites); intuition.
    generalize (H1 x); intuition.
    match goal with
      | [ |- ?X = _ ] => destruct X
    end; intuition.

    left.
    destruct (AllS_not (f := fun v : VarMap.dom =>
      AllocSet.incl (VarMap.sel (avars abs1) v)
      (VarMap.sel (avars abs2) v) = true) checkVar H); intuition.
    destruct H1 as [x [Hin Hincl]].
    exists x; simpl; intuition.
    generalize (VarSet.elems_ok procVars); intuition.
    generalize (H1 x); intuition.
    match goal with
      | [ |- ?X = _ ] => destruct X
    end; intuition.
  Qed.

  Hint Resolve approxStrict_dec.

  Fixpoint distinct (ls : list (nat*bool)%type) : Prop :=
    match ls with
      | nil => True
      | h::t => AllS (fun x => x <> h) t
	/\ distinct t
    end.

  Definition cardinality_atLeast s n :=
    exists ls, distinct ls
      /\ length ls = n
      /\ AllS (fun x => AllocSet.In x s = true) ls.

  Definition cardinality s n :=
    cardinality_atLeast s n
    /\ forall n', cardinality_atLeast s n' -> n' <= n.

  Definition bound := length (AllocSet.elems procSites).

  Definition neq_nat_dec : forall (x y : nat), {x <> y} + {~x <> y}.
    intros.
    destruct (eq_nat_dec x y); auto.
  Defined.

  Definition neq_natboolpair_dec : forall (x y : (nat*bool)%type), {x <> y} + {~x <> y}.
    intros.
    destruct (eq_natboolpair_dec x y); auto.
  Defined.

  Definition distinctify : forall (ls : list object),
    {ls' : list object | distinct ls'
      /\ forall x, In x ls <-> In x ls'}.
    induction ls.

    exists (@nil object); simpl; intuition.

    destruct IHls as [ls' [Hdist Hin]].
    destruct (AllS_dec (fun x => x <> a) (fun x => neq_natboolpair_dec x a) ls').
    exists (a :: ls'); simpl; firstorder.

    destruct (AllS_not_dec (f := fun x : object => x <> a)
      (fun x => neq_natboolpair_dec x a) n) as [[x [Hin' Hnot]] | Heq].
    exists ls'; intuition.
    generalize (Hin x0); intuition.
    simpl in H; intuition; subst.
    replace x0 with x; auto.
    destruct (eq_natboolpair_dec x x0); intuition.

    simpl; right.
    generalize (Hin x0); intuition.

    rewrite Heq in n.
    wrong.
    intuition.
  Qed.

  Lemma incl_S : forall (ls : list object) a,
    In a ls
    -> exists ls', length ls = S (length ls')
      /\ incl ls (a :: ls').
    induction ls; simpl; intuition; subst.
    firstorder.
    
    destruct (eq_natboolpair_dec a0 a); subst.
    exists ls; intuition.

    destruct (IHls _ H0) as [ls' [Hlen Hincl]].
    rewrite Hlen.
    exists (a :: ls'); intuition.
    red; intros.
    red in Hincl.
    simpl in *; intuition.
    destruct (Hincl a1); intuition.
  Qed.

  Lemma incl_trans' : forall (a : object) ls1 ls2,
    distinct (a :: ls1)
    -> incl (a :: ls1) (a :: ls2)
    -> incl ls1 ls2.
    unfold incl; intuition.
    destruct (eq_natboolpair_dec a0 a); subst.
    simpl in H; intuition.
    wrong.

    generalize H1 H2; clear_all.
    induction ls1; simpl; intuition; subst.
    generalize (AllS_In H2 a); intuition.
    apply H0.
    inversion H2; intuition.

    assert (In a0 (a :: ls2)).
    intuition.
    inversion H2; intuition.
    wrong; intuition.
  Qed.

  Lemma incl_length : forall (ls1 ls2 : list object),
    incl ls1 ls2
    -> distinct ls1
    -> length ls1 <= length ls2.
    induction ls1.

    destruct ls2; intuition.
    
    simpl; intuition.
    assert (exists ls', length ls2 = S (length ls')
      /\ incl ls2 (a :: ls')).
    apply incl_S; intuition.
    destruct H0 as [ls' [Hlen Hincl]].
    rewrite Hlen.
    generalize (IHls1 ls'); intuition.
    assert (incl ls1 ls').
    apply incl_trans' with a; simpl; intuition.
    red; intros.
    inversion H3; subst; intuition.
    
    intuition.
  Qed.

  Lemma card : forall s,
    AllocSet.incl s procSites = true
    -> {n : nat
      | n <= bound
	/\ cardinality s n}.
    intros.
    destruct (distinctify (AllocSet.elems s)) as [ls [Hdis Hin]].
    exists (length ls); intuition.

    unfold bound.
    generalize (AllocSet.elems_ok procSites).
    generalize (AllocSet.elems_ok s).
    generalize (AllocSet.incl_ok s procSites).
    intuition.
    apply incl_length; auto.
    red; intros.
    generalize (Hin a); intuition.
    generalize (H1 a).
    generalize (H0 a).
    generalize (H2 a).
    intuition.

    split.
    unfold cardinality_atLeast.
    exists ls; intuition.
    apply AllS_deduce; intuition.
    generalize (AllocSet.elems_ok s); intuition.
    generalize (H1 x); intuition.
    apply H3.
    generalize (Hin x); intuition.

    intros.
    destruct H0 as [ls' Hls']; intuition.
    rewrite <- H2.
    apply incl_length; auto.
    red; intros.
    generalize (AllS_In H3 _ H1); intro Hin'.
    generalize (AllocSet.elems_ok s).
    generalize (Hin a); intuition.
    apply H6.
    generalize (H5 a); intuition.
  Qed.

  Definition var_bounded : forall (abs : astate) (v : var),
    AllocSet.incl (VarMap.sel (avars (proj1_sig abs)) v) procSites = true.
    destruct abs as [abs Habs]; simpl.
    inversion Habs.
    eauto.
  Qed.

  Definition site_bounded : forall (abs : astate) (s : object),
    AllocSet.incl (AllocMap.sel (aheap (proj1_sig abs)) (cell s)) procSites = true.
    destruct abs as [abs Habs]; simpl.
    inversion Habs.
    eauto.
  Qed.

  Definition size (abs : astate) :=
    fold_right plus 0 (map (fun x => proj1_sig (card _ (var_bounded abs x)))
      (VarSet.elems procVars))
    + fold_right plus 0 (map (fun x => proj1_sig (card _ (site_bounded abs x)))
      (AllocSet.elems procSites)).

  Lemma mult_le : forall n x m y,
    x <= n * m
    -> y <= n
    -> y + x <= n * S m.
    intros.
    replace (n * S m) with (n + n * m).
    omega.

    clear_all.
    induction n; simpl; intuition.
  Qed.
  
  Lemma size_bounded : forall abs,
    size abs <= bound * (length (VarSet.elems procVars) + length (AllocSet.elems procSites)).
    unfold size.
    intros.
    induction (VarSet.elems procVars); simpl.

    induction (AllocSet.elems procSites); simpl; intuition.

    destruct (card (AllocMap.sel (aheap (proj1_sig abs)) (cell a)) (site_bounded abs a)); simpl; intuition.
    apply mult_le; trivial.

    destruct (card (VarMap.sel (avars (proj1_sig abs)) a) (var_bounded abs a)); simpl; intuition.
    generalize (mult_le _ _ _ _ IHl H).
    omega.
  Qed.

  Lemma fold_plus_ge : forall (A : Set) f1 f2 (ls : list A),
    AllS (fun x => f1 x >= f2 x) ls
    -> fold_right plus 0 (map f1 ls) >= fold_right plus 0 (map f2 ls).
    induction ls; simpl; intuition.
    inversion H; subst.
    intuition.
  Qed.

  Lemma fold_plus_gt : forall (A : Set) f1 f2 x,
    f1 x > f2 x
    -> forall (ls : list A), AllS (fun x => f1 x >= f2 x) ls
    -> In x ls
    -> fold_right plus 0 (map f1 ls) > fold_right plus 0 (map f2 ls).
    induction ls; simpl; intuition; subst.
    
    inversion H0; subst.
    generalize (fold_plus_ge _ _ _ _ H4).
    omega.

    inversion H0; subst.
    intuition.
  Qed.
  
  Lemma sum_plus1 : forall x1 y1 x2 y2,
    x1 > x2
    -> y1 >= y2
    -> x1 + y1 > x2 + y2.
    intros; omega.
  Qed.

  Lemma sum_plus2 : forall x1 y1 x2 y2,
    x1 >= x2
    -> y1 > y2
    -> x1 + y1 > x2 + y2.
    intros; omega.
  Qed.

  Lemma incl_cardinality' : forall s1 s2 c,
    AllocSet.incl s1 s2 = true
    -> cardinality_atLeast s1 c
    -> cardinality_atLeast s2 c.
    intros.
    destruct H0 as [ls]; intuition.
    exists ls; intuition.
    apply AllS_deduce; intros.
    apply AllocSet.incl_In with s1; auto.
    apply (AllS_In H3); auto.
  Qed.

  Lemma incl_cardinality : forall s1 s2 c1 c2,
    AllocSet.incl s1 s2 = true
    -> cardinality s1 c1
    -> cardinality s2 c2
    -> c1 <= c2.
    intros.
    destruct H0.
    destruct H1.
    destruct H1 as [ls2 Hls2].
    intuition.
    
    apply H3.
    eapply incl_cardinality'; eauto.
  Qed.

  Lemma incl_gt : forall (x : object) ls2 ls1,
    distinct ls1
    -> incl ls1 ls2
    -> ~In x ls1
    -> In x ls2
    -> length ls2 > length ls1.
    intros.

    assert (incl (x :: ls1) ls2).
    red; simpl; intuition; subst; trivial.

    assert (length (x :: ls1) <= length ls2).
    apply incl_length; trivial.
    split; intuition.
    apply AllS_deduce.
    intros; subst; auto.

    simpl in H4.
    unfold object in H4.
    omega.
  Qed.

  Lemma set_grew : forall s1 s2 c1 c2,
    AllocSet.incl s2 s1 = true
    -> AllocSet.incl s1 s2 = false
    -> cardinality s1 c1
    -> cardinality s2 c2
    -> c1 > c2.
    intros.

    destruct (AllocSet.incl_false H0) as [x [Hyes Hno]].

    destruct H1.
    destruct H2.
    destruct H1 as [ls1 Hls1].
    destruct H2 as [ls2 Hls2]; intuition.

    subst.
    apply incl_gt with x; eauto.
    
    red; intros.
    destruct (In_dec eq_natboolpair_dec a ls1); trivial.
    wrong.
    assert (S (length ls1) <= length ls1).
    apply H3.
    exists (a :: ls1); simpl; intuition.
    apply AllS_deduce.
    intros; subst; auto.
    constructor; intuition.
    apply AllocSet.incl_In with s2; auto.
    apply (AllS_In H9); auto.
    omega.

    intro.
    assert (AllocSet.In x s2 = true).
    apply (AllS_In H9); auto.
    congruence.

    destruct (In_dec eq_natboolpair_dec x ls1); trivial.
    wrong.
    assert (S (length ls1) <= length ls1).
    apply H3.
    exists (x :: ls1); simpl; intuition.
    apply AllS_deduce.
    intros; subst; auto.
    omega.
  Qed.

  Lemma size_approxStrict : forall abs abs',
    approxStrict abs abs'
    -> size abs > size abs'.
    destruct abs as [abs Habs].
    destruct abs' as [abs' Habs'].
    intros.
    destruct H; simpl in *; idtac.
    destruct strictNe0 as [[v [Hin Hincl]] | [a [Hin Hincl]]].

    unfold size; simpl.
    apply sum_plus1.

    apply fold_plus_gt with v.
    destruct (card (VarMap.sel (avars abs) v)
      (var_bounded (exist valid abs Habs) v)) as [c1 [Hlt1 Hc1]]; simpl.
    destruct (card (VarMap.sel (avars abs') v)
      (var_bounded (exist valid abs' Habs') v)) as [c2 [Hlt2 Hc2]]; simpl.
    apply set_grew with (VarMap.sel (avars abs) v) (VarMap.sel (avars abs') v); intuition.
    destruct strictApprox0.
    simpl in *; auto.

    apply AllS_deduce; intros.
    destruct (card (VarMap.sel (avars abs) x)
      (var_bounded (exist valid abs Habs) x)) as [c1 [Hlt1 Hc1]]; simpl.
    destruct (card (VarMap.sel (avars abs') x)
      (var_bounded (exist valid abs' Habs') x)) as [c2 [Hlt2 Hc2]]; simpl.
    assert (c2 <= c1); try omega.
    apply incl_cardinality with (VarMap.sel (avars abs') x) (VarMap.sel (avars abs) x); auto.
    destruct strictApprox0.
    simpl in *; auto.

    generalize (VarSet.elems_ok procVars); intuition.
    generalize (H v); intuition.

    apply fold_plus_ge.
    apply AllS_deduce; intros.
    destruct (card (AllocMap.sel (aheap abs) (cell x))
      (site_bounded (exist valid abs Habs) x)) as [c1 [Hlt1 Hc1]]; simpl.
    destruct (card (AllocMap.sel (aheap abs') (cell x))
        (site_bounded (exist valid abs' Habs') x)) as [c2 [Hlt2 Hc2]]; simpl.
    assert (c2 <= c1); try omega.
    apply incl_cardinality with (AllocMap.sel (aheap abs') (cell x)) (AllocMap.sel (aheap abs) (cell x)); auto.
    destruct strictApprox0.
    simpl in *; auto.

    unfold size; simpl.
    apply sum_plus2.

    apply fold_plus_ge.
    apply AllS_deduce; intros.
    destruct (card (VarMap.sel (avars abs) x)
      (var_bounded (exist valid abs Habs) x)) as [c1 [Hlt1 Hc1]]; simpl.
    destruct (card (VarMap.sel (avars abs') x)
        (var_bounded (exist valid abs' Habs') x)) as [c2 [Hlt2 Hc2]]; simpl.
    assert (c2 <= c1); try omega.
    apply incl_cardinality with (VarMap.sel (avars abs') x) (VarMap.sel (avars abs) x); auto.
    destruct strictApprox0.
    simpl in *; auto.

    apply fold_plus_gt with a.
    destruct (card (AllocMap.sel (aheap abs) (cell a))
        (site_bounded (exist valid abs Habs) a)) as [c1 [Hlt1 Hc1]]; simpl.
    destruct (card (AllocMap.sel (aheap abs') (cell a))
        (site_bounded (exist valid abs' Habs') a)) as [c2 [Hlt2 Hc2]]; simpl.
    apply set_grew with (AllocMap.sel (aheap abs) (cell a)) (AllocMap.sel (aheap abs') (cell a)); intuition.
    destruct strictApprox0.
    simpl in *; auto.

    apply AllS_deduce; intros.
    destruct (card (AllocMap.sel (aheap abs) (cell x))
      (site_bounded (exist valid abs Habs) x)) as [c1 [Hlt1 Hc1]]; simpl.
    destruct (card (AllocMap.sel (aheap abs') (cell x))
      (site_bounded (exist valid abs' Habs') x)) as [c2 [Hlt2 Hc2]]; simpl.
    assert (c2 <= c1); try omega.
    apply incl_cardinality with (AllocMap.sel (aheap abs') (cell x)) (AllocMap.sel (aheap abs) (cell x)); auto.
    destruct strictApprox0.
    simpl in *; auto.

    generalize (AllocSet.elems_ok procSites); intuition.
    generalize (H a); intuition.
  Qed.

  Lemma approxStrict_wf' : forall n a,
    bound * (length (VarSet.elems procVars) + length (AllocSet.elems procSites)) - size a <= n
    -> Acc approxStrict a.
    induction n; simpl; intuition.

    constructor; intros.
    wrong.
    generalize (size_approxStrict _ _ H0).
    generalize (size_bounded y).
    omega.

    constructor; intros.
    apply IHn.
    generalize (size_approxStrict _ _ H0).
    omega.
  Qed.

  Theorem approxStrict_wf : well_founded approxStrict.
    intro.
    eapply approxStrict_wf'; eauto.
  Qed.

  Hint Resolve approxStrict_wf.

  Lemma ainstr_widen_sequence_left : forall c1 c2,
    {a : abstract_activity | In a (activities c1) }
    -> {a: abstract_activity | In a (activities (Sequence c1 c2))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.
  Lemma ainstr_widen_sequence_right : forall c1 c2,
    {a : abstract_activity | In a (activities c2) }
    -> {a: abstract_activity | In a (activities (Sequence c1 c2))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.
  Lemma ainstr_widen_choice_left : forall c1 c2,
    {a : abstract_activity | In a (activities c1) }
    -> {a: abstract_activity | In a (activities (Choice c1 c2))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.
  Lemma ainstr_widen_choice_right : forall c1 c2,
    {a : abstract_activity | In a (activities c2) }
    -> {a: abstract_activity | In a (activities (Choice c1 c2))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.
  Lemma ainstr_widen_parallel_left : forall c1 c2,
    {a : abstract_activity | In a (activities c1) }
    -> {a: abstract_activity | In a (activities (Parallel c1 c2))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.
  Lemma ainstr_widen_parallel_right : forall c1 c2,
    {a : abstract_activity | In a (activities c2) }
    -> {a: abstract_activity | In a (activities (Parallel c1 c2))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.
  Lemma ainstr_widen_loop : forall c,
    {a : abstract_activity | In a (activities c) }
    -> {a: abstract_activity | In a (activities (Loop c))}.
    intros.
    destruct H as [a Hins].
    simpl; eauto.
  Defined.

 Fixpoint traverse (A: Set) (B: Set) (f: A -> B) (proc: compound_activity A) {struct proc} :=
   match proc with
     | Activity a => Activity (f a)
     | Sequence c1 c2 => Sequence (traverse A B f c1) (traverse A B f c2)
     | Choice c1 c2 => Choice (traverse A B f c1) (traverse A B f c2)
     | Loop c => Loop (traverse A B f c)
     | Parallel c1 c2 => Parallel (traverse A B f c1) (traverse A B f c2)
     | Empty => Empty B
    end.

  Definition aprocess' : forall (proc : compound_activity abstract_activity),
    compound_activity (sig (fun a => In a (activities proc))).
    refine (fix aprocess' (proc : compound_activity abstract_activity)
      : compound_activity (sig (fun a => In a (activities proc))) :=
      match proc return compound_activity (sig (fun a => In a (activities proc))) with
        | Activity a => Activity (exist _ a _)
        | Sequence c1 c2 => Sequence (traverse _ _ (ainstr_widen_sequence_left c1 c2) (aprocess' c1))
                                     (traverse _ _ (ainstr_widen_sequence_right c1 c2) (aprocess' c2))
        | Choice c1 c2 => Choice (traverse _ _ (ainstr_widen_choice_left c1 c2) (aprocess' c1))
                                 (traverse _ _ (ainstr_widen_choice_right c1 c2) (aprocess' c2))
        | Loop c => Loop (traverse _ _ (ainstr_widen_loop c) (aprocess' c))
        | Parallel c1 c2 => Parallel (traverse _ _ (ainstr_widen_parallel_left c1 c2) (aprocess' c1))
                                     (traverse _ _ (ainstr_widen_parallel_right c1 c2) (aprocess' c2))
        | Empty => Empty _
      end).
      simpl; eauto.
  Defined.

  Definition aprocess := aprocess' process.

  Definition fixed_point' : {fp : astate
    | forall s, reachable_flowInsensitive aexec aprocess ainitState s
      -> approx s fp}.
    apply fixed_point with (approxStrict := approxStrict); eauto.
  Qed.

  Lemma In_traverse : forall ( A B : Set) x (f : A -> B) c,
    In x (activities c)
    -> In (f x) (activities (traverse A B f c)). 
    induction c. 
    simpl; intuition; try subst; eauto.

    simpl; intuition.
    apply in_or_app.
    apply in_app_or in H.
    destruct H; eauto.

    simpl; intuition.
    apply in_or_app.
    apply in_app_or in H.
    destruct H; eauto.

    simpl; intuition; eauto.

    simpl; intuition.
    apply in_or_app.
    apply in_app_or in H.
    destruct H; eauto.

    eauto.
  Qed.    


  Lemma in_aprocess' : forall a proc,
    In a (activities proc) ->
    exists H : In a (activities proc),
      In (exist (fun a0 => In a0 (activities proc)) a H)
      (activities (aprocess' proc)).
    induction proc; intros.

    inversion H.
    subst; simpl; eauto.
    wrong; eauto.

    simpl in H.
    apply in_app_or in H.
    destruct H.

    simpl.
    destruct (IHproc1 H) as [h Hh].
    caseEq (ainstr_widen_sequence_left proc1 proc2 (exist (fun a => In a (activities proc1)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists (in_or_app (activities proc1) (activities proc2) _ (or_introl h)).
    simpl.
    assert (In x (activities proc1) \/ In x (activities proc2)); eauto.
    apply in_or_app. left.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc1)) x h)
      (ainstr_widen_sequence_left proc1 proc2)).
    trivial.

    simpl.
    destruct (IHproc2 H) as [h Hh].
    caseEq (ainstr_widen_sequence_right proc1 proc2 (exist (fun a => In a (activities proc2)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists (in_or_app (activities proc1) (activities proc2) _ (or_intror h)).
    simpl.
    assert (In x (activities proc1) \/ In x (activities proc2)); eauto.
    apply in_or_app. right.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc2)) x h)
      (ainstr_widen_sequence_right proc1 proc2)).
    trivial.

    simpl in H.
    apply in_app_or in H.
    destruct H.

    simpl.
    destruct (IHproc1 H) as [h Hh].
    caseEq (ainstr_widen_choice_left proc1 proc2 (exist (fun a => In a (activities proc1)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists (in_or_app (activities proc1) (activities proc2) _ (or_introl h)).
    simpl.
    assert (In x (activities proc1) \/ In x (activities proc2)); eauto.
    apply in_or_app. left.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc1)) x h)
      (ainstr_widen_choice_left proc1 proc2)).
    trivial.

    simpl.
    destruct (IHproc2 H) as [h Hh].
    caseEq (ainstr_widen_choice_right proc1 proc2 (exist (fun a => In a (activities proc2)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists (in_or_app (activities proc1) (activities proc2) _ (or_intror h)).
    simpl.
    assert (In x (activities proc1) \/ In x (activities proc2)); eauto.
    apply in_or_app. right.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc2)) x h)
      (ainstr_widen_choice_right proc1 proc2)).
    trivial.

    simpl in H.
    simpl.
    destruct (IHproc H) as [h Hh].
    caseEq (ainstr_widen_loop proc (exist (fun a => In a (activities proc)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists h.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc)) x h)
      (ainstr_widen_loop proc)).
    trivial.

    simpl in H.
    apply in_app_or in H.
    destruct H.

    simpl.
    destruct (IHproc1 H) as [h Hh].
    caseEq (ainstr_widen_parallel_left proc1 proc2 (exist (fun a => In a (activities proc1)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists (in_or_app (activities proc1) (activities proc2) _ (or_introl h)).
    simpl.
    assert (In x (activities proc1) \/ In x (activities proc2)); eauto.
    apply in_or_app. left.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc1)) x h)
      (ainstr_widen_parallel_left proc1 proc2)).
    trivial.

    simpl.
    destruct (IHproc2 H) as [h Hh].
    caseEq (ainstr_widen_parallel_right proc1 proc2 (exist (fun a => In a (activities proc2)) _ h)); intros.
    simpl in H0.
    injection H0; intros; subst.
    exists (in_or_app (activities proc1) (activities proc2) _ (or_intror h)).
    simpl.
    assert (In x (activities proc1) \/ In x (activities proc2)); eauto.
    apply in_or_app. right.
    apply (In_traverse _ _ (exist (fun a : abstract_activity => In a (activities proc2)) x h)
      (ainstr_widen_parallel_right proc1 proc2)).
    trivial.

    wrong. eauto.
  Qed.

  Lemma in_aprocess : forall a,
    In a (activities process)
      -> exists H, In (exist (fun a => In a (activities process)) a H) (activities aprocess).
    unfold aprocess; intros.
    apply in_aprocess'; trivial.
  Qed.

  Axiom valid_irrel : forall s (H1 H2 : valid s), H1 = H2.

  Lemma reachable_a : forall s1 (Hv1 : valid s1) s2,
    reachable_flowInsensitive abstract_exec process s1 s2
      -> exists Hv2 : valid s2,
	reachable_flowInsensitive aexec aprocess (exist _ _ Hv1) (exist _ _ Hv2).
    induction 1; intuition eauto.
    subst.

    assert (valid (abstract_exec a s1)).
    apply aexec'; trivial.
    destruct (IHreachable_flowInsensitive H0) as [Hv2 Hreach].
    exists Hv2.

    destruct (in_aprocess _ H) as [Hin HHin].

    eapply Rfi_Step; eauto.
    unfold aexec at 2; simpl.
    rewrite (valid_irrel _ (aexec' a s1 Hin Hv1) H0).    
    trivial.
  Qed.

  Definition fixed_point : {fp : abstract_state
    | forall s, reachable_flowInsensitive abstract_exec process abstract_initState s
      -> approx' s fp}.
    destruct fixed_point' as [[fp Hvalid] Hfp].
    exists fp; intros.

    assert (valid abstract_initState).
    unfold abstract_initState; intuition.
    split; simpl; intuition.
    rewrite VarMap.sel_init; eauto.
    rewrite AllocMap.sel_init; eauto.
    rewrite VarMap.sel_init; eauto.
    rewrite AllocMap.sel_init; eauto.

    destruct (reachable_a _ (proj2_sig ainitState) _ H).
    generalize (Hfp _ H1); intros.
    trivial.
  Qed.
End fixed_point.

