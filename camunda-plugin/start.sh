#!/bin/bash
if [ ! -e camunda-modeler ]
then
        wget -N  https://camunda.org/release/camunda-modeler/1.16.2/camunda-modeler-1.16.2-linux-x64.tar.gz
        tar -xvf camunda-modeler-1.16.2-linux-x64.tar.gz
fi

CAMUNDA_PLUGIN_DIRECTORY="./camunda-modeler/plugins/"

docker run --rm -w ${PWD}/static-analysis-plugin --user $(id -u) -v $HOME:$HOME node npm install
docker run --rm -w ${PWD}/static-analysis-plugin --user $(id -u) -v $HOME:$HOME node npm run build

echo "Link setzen"
ln -vsf ${PWD}/static-analysis-plugin  ${CAMUNDA_PLUGIN_DIRECTORY}


( cd DockerAnalysisServer/; docker build -t analysis-server . || docker build -t analysis-server --no-cache . )
docker run --rm -w ${PWD}/static-analysis-plugin/server  --user $( id -u ) -p 1511:1511  -v $HOME:$HOME analysis-server python3 server.py & 

camunda-modeler/camunda-modeler sample.bpmn
