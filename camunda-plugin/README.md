# Camunda Static Analysis Plugin

## Description
A static information flow analysis plugin for [Camunda Modeler](https://camunda.com/products/modeler/)

## Installation
We recommend to use a recent Ubuntu system for using the plugin.

1. Install Docker, see [Get Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. Run `start.sh`

## Build Process
Running `start.sh` will do the following:

1. Download the Camunda Modeler, if not already done
2. Compile the plugin using [Node](https://hub.docker.com/_/node/)
3. Set a symbolic link from the `plugins` folder of the Camunda Modeler
to the folder `static-analysis-plugin`, which contains the source code for the plugin
4. Build the docker image `analysis-plugin`, following the instructions in `DockerAnalysisServer/Dockerfile`
5. Start the analysis server
6. Start Camunda Modeler
