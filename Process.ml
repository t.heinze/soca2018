let request = O
let score = S request 
let rating = S score
let reply = S rating

let risk = O

let process = Sequence (
                      (Activity (Allocate request)),
                      (Sequence (
                        (Activity (Source score)),
                        (Sequence (
                          (Activity (Write (request, risk, score))),
                          (Sequence (
                            (Choice (
                              (Activity (Read (rating, request, risk))),
                              (Activity (Allocate rating)))),
                            (Sequence (
                              (Activity (Sanitize (reply, rating))),
                              Empty)))))))))

let result2 = (match analysis process reply with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")
let result1 = (match analysis process rating with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")

let () = Printf.printf "Variable rating: %s, variable reply: %s\n" result1 result2
