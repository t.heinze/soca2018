MODS        := Tactics ListUtil Maps Machine Pointer AndersenModel AndersenSound AndersenIter Andersen
COQ_SOURCES := $(MODS:%=%.v)

include Makefile.coq

Makefile.coq: Makefile $(COQ_SOURCES)
	coq_makefile $(COQ_SOURCES) COQC = "coqc" >Makefile.coq

Andersen.ml: Andersen.vo
	coqc AndersenExtract >Andersen.ml
