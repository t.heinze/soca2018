Require Import List.
Require Import Machine Maps Pointer.

Set Implicit Arguments.

Inductive abstract_activity : Set :=
  | AbsAllocate : var -> allocation_site -> abstract_activity
  | AbsSource : var -> allocation_site -> abstract_activity
  | AbsCopy : var -> var -> abstract_activity
  | AbsSanitize : var -> var -> abstract_activity
  | AbsRead : var -> var -> field -> abstract_activity
  | AbsWrite : var -> field -> var -> abstract_activity.

Record abstract_state : Set := {
  avars : VarMap.map AllocSet.set;
  aheap : AllocMap.map AllocSet.set
}.

Definition abstract_exec (i : abstract_activity) (s : abstract_state)
  : abstract_state :=
  match i with
    | AbsAllocate dst site =>
      Build_abstract_state
      (VarMap.upd (avars s) dst
	(AllocSet.add (VarMap.sel (avars s) dst) (site, false)))
      (aheap s)
    | AbsSource dst site =>
      Build_abstract_state
      (VarMap.upd (avars s) dst
	(AllocSet.add (VarMap.sel (avars s) dst) (site, true)))
      (aheap s)
    | AbsCopy dst src =>
      Build_abstract_state
      (VarMap.upd (avars s) dst
	(AllocSet.union (VarMap.sel (avars s) dst) (VarMap.sel (avars s) src)))
      (aheap s)
    | AbsSanitize dst src =>
      Build_abstract_state
      (VarMap.upd (avars s) dst
	(fold_left (fun known site => AllocSet.union known (AllocSet.add AllocSet.empty ((cell site), false)))
	  (AllocSet.elems (VarMap.sel (avars s) src)) (VarMap.sel (avars s) dst)))
      (aheap s)
    | AbsRead dst src fl =>
      Build_abstract_state
      (VarMap.upd (avars s) dst
	(fold_left (fun known site => AllocSet.union known (AllocMap.sel (aheap s) ((cell site), fl)))
	  (AllocSet.elems (VarMap.sel (avars s) src)) (VarMap.sel (avars s) dst)))
      (aheap s)
    | AbsWrite dst fl src =>
      Build_abstract_state
      (avars s)
      (fold_left (fun known site => AllocMap.upd known ((cell site), fl)
	(AllocSet.union (AllocMap.sel (aheap s) ((cell site), fl)) (VarMap.sel (avars s) src)))
      (AllocSet.elems (VarMap.sel (avars s) dst)) (aheap s))
  end.

Fixpoint abstractProcess' (site : allocation_site)
  (process : compound_activity activity) {struct process}
  : compound_activity abstract_activity :=
  match process with
    | Activity a =>
      match a with
	| Allocate dst => Activity (AbsAllocate dst site)
	| Source dst => Activity (AbsSource dst site)
	| Copy dst src => Activity (AbsCopy dst src)
	| Sanitize dst src => Activity (AbsSanitize dst src)
	| Read dst src fl => Activity (AbsRead dst src fl)
	| Write dst fl src => Activity (AbsWrite dst fl src)
      end
    | Sequence c1 c2 => Sequence (abstractProcess' (S site) c1) (abstractProcess' (S site) c2)
    | Choice c1 c2 => Choice (abstractProcess' (S site) c1) (abstractProcess' (S site) c2)
    | Loop c => Loop (abstractProcess' (S site) c)
    | Parallel c1 c2 => Parallel (abstractProcess' (S site) c1) (abstractProcess' (S site) c2)
    | Empty => Empty abstract_activity
  end.

Definition abstractProcess (process: compound_activity activity) := abstractProcess' 1 process.
