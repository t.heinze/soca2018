Require Import Arith List Omega.
Require Import ListUtil Maps Tactics.
Require Import Machine AndersenModel Pointer.

Set Implicit Arguments.

Inductive followPath : var -> nat -> list field -> state -> object -> Prop :=
  | Path_Done : forall v s,
    followPath v 0 nil s (VarMap.sel (vars s) v)
  | Path_First_Step: forall v s v' f',
    followPath v 0 nil s v'
    -> cell v' <> 0
    -> followPath v 1 (f'::nil) s (NatPairMap.sel (heap s) ((cell v'), f'))
  | Path_Step : forall v n s v' fl f',
    followPath v (S n) fl s v'
    -> cell v' <> 0
    -> followPath v (S (S n)) (f'::fl) s (NatPairMap.sel (heap s) ((cell v'), f')).

Inductive abstract_followPath
  : var -> nat -> list field -> abstract_state -> object -> Prop :=
  | APath_Done : forall v s a,
    AllocSet.In a (VarMap.sel (avars s) v) = true
    -> abstract_followPath v 0 nil s a
  | APath_First_Step : forall v s v' a f',
    abstract_followPath v 0 nil s v'
    -> AllocSet.In a (AllocMap.sel (aheap s) ((cell v'), f')) = true
    -> abstract_followPath v 1 (f'::nil) s a
  | APath_Step : forall v n s v' a fl f',
    abstract_followPath v (S n) fl s v'
    -> AllocSet.In a (AllocMap.sel (aheap s) ((cell v'), f')) = true
    -> abstract_followPath v (S (S n)) (f'::fl) s a.

Hint Constructors followPath abstract_followPath.

Definition cellCompatible (conc : state) (abs : abstract_state)
  v1 n1 v2 n2 fl1 fl2 :=
  forall r1 r2, followPath v1 n1 fl1 conc r1
    -> cell r1 <> 0
    -> followPath v2 n2 fl2 conc r2
    -> cell r1 = cell r2
    -> exists r1' r2',
      abstract_followPath v1 n1 fl1 abs r1'
      /\ abstract_followPath v2 n2 fl2 abs r2'
      /\ cell r1' = cell r2'.

Definition allCellCompatible conc abs :=
  forall v1 n1 v2 n2 fl1 fl2, cellCompatible conc abs v1 n1 v2 n2 fl1 fl2.

Definition taintCompatible (conc : state) (abs: abstract_state)
  v n fl :=
  forall r, followPath v n fl conc r
    -> cell r <> 0
    -> taint r = true
    -> exists r', abstract_followPath v n fl abs r'
       /\ taint r' = true.

Definition allTaintCompatible conc abs :=
  forall v n fl, taintCompatible conc abs v n fl.

Record compatible (conc : state) (abs : abstract_state) : Prop := {
  compatInitial : forall f, NatPairMap.sel (heap conc) (0, f) = (0, false);
  compatInBounds : forall v n v' fl, followPath v n fl conc v' -> cell v' <= limit conc;
  compatZeroed : forall a f, a > limit conc -> NatPairMap.sel (heap conc) (a, f) = (0, false);
  compatCell : allCellCompatible conc abs;
  compatTaint: allTaintCompatible conc abs
}.

Hint Constructors reachable_flowInsensitive.

Hint Resolve in_or_app.

Lemma abstractAllocate' : forall v process next,
  In (Allocate v) (activities process)
    -> exists site, In (AbsAllocate v site) (activities (abstractProcess' next process)).
  induction process; simplify; apply in_app_or in H; firstorder.
  eapply IHprocess1 in H; destruct H; exists x; eauto.
  eapply IHprocess2 in H; destruct H; exists x; eauto.
  eapply IHprocess1 in H; destruct H; exists x; eauto.
  eapply IHprocess2 in H; destruct H; exists x; eauto.
  eapply IHprocess1 in H; destruct H; exists x; eauto.
  eapply IHprocess2 in H; destruct H; exists x; eauto.
Qed.

Lemma abstractAllocate : forall v process,
  In (Allocate v) (activities process)
    -> exists site, In (AbsAllocate v site) (activities (abstractProcess process)).
  intros; unfold abstractProcess; apply abstractAllocate'; trivial.
Qed.

Lemma abstractSource' : forall v process next,
  In (Source v) (activities process)
    -> exists site, In (AbsSource v site) (activities (abstractProcess' next process)).
  induction process; simplify.
  apply in_app_or in H; firstorder.
  eapply IHprocess1 in H; destruct H; exists x; eauto.
  eapply IHprocess2 in H; destruct H; exists x; eauto.
  apply in_app_or in H; firstorder.
  eapply IHprocess1 in H; destruct H; exists x; eauto.
  eapply IHprocess2 in H; destruct H; exists x; eauto.
  apply in_app_or in H; firstorder.
  eapply IHprocess1 in H; destruct H; exists x; eauto.
  eapply IHprocess2 in H; destruct H; exists x; eauto.
Qed.

Lemma abstractSource : forall v process,
  In (Source v) (activities process)
    -> exists site, In (AbsSource v site) (activities (abstractProcess process)).
  intros; unfold abstractProcess; apply abstractSource'; trivial.
Qed.

Ltac VarMap_split :=
  match goal with
    | [ |- context[VarMap.sel (VarMap.upd ?M ?A ?V) ?A'] ] =>
      let Haddr := fresh "Haddr" with Heq := fresh "Heq" in
	(destruct (VarMap.sel_upd M A V A') as [[Haddr Heq] | [Haddr Heq]];
	  rewrite Heq; simplify)
  end.

Ltac nat_split :=
  match goal with
    | [ |- context[match ?N with O => _ | S _ => _ end] ] => destruct N; simplify
  end.

Hint Rewrite VarMap.sel_upd_eq : Maps.
Hint Rewrite VarMap.sel_upd_neq using (intuition; fail) : Maps.

Ltac mySimplify := repeat progress (simplify;
  autorewrite with Maps;
    try match goal with
	  | [ H : _ |- _ ] =>
	    rewrite VarMap.sel_upd_eq in H
	      || (rewrite VarMap.sel_upd_eq in H; [idtac | intuition; fail])
	end).

Hint Resolve AllocSet.In_add_eq.

Lemma cellCompatible_symm : forall conc abs v1 n1 v2 n2 fl1 fl2,
  cellCompatible conc abs v1 n1 v2 n2 fl1 fl2
  -> cellCompatible conc abs v2 n2 v1 n1 fl2 fl1.
  unfold cellCompatible. firstorder.
  assert (cell r2 <> 0); rewrite H3 in H1; eauto.
  symmetry in H3.
  destruct (H _ _ H2 H4 H0 H3) as [r2' [r1' [Hapath1 [Hapath2 Hcell]]]].
  exists r1'; exists r2'; auto.
Qed.

Hint Resolve cellCompatible_symm.

Lemma followPath_write_var : forall v v' conc' n r fl,
  v <> v'
  -> followPath v' n fl conc' r
  -> forall conc addr lim, conc' = Build_state (VarMap.upd (vars conc) v addr) (heap conc) lim
    -> followPath v' n fl conc r.
  induction 2; mySimplify.
Qed.

Hint Resolve followPath_write_var.

Lemma abstract_followPath_write_var : forall v v' n abs r fl,
  v <> v'
  -> abstract_followPath v' n fl abs r
  -> forall site, abstract_followPath v' n fl
    (Build_abstract_state (VarMap.upd (avars abs) v site) (aheap abs)) r.
  induction 2; mySimplify.
  constructor; mySimplify.
Qed.

Hint Resolve abstract_followPath_write_var.

Lemma compatible_write_var : forall conc abs v addr site lim,
  let conc' := Build_state
    (VarMap.upd (vars conc) v addr)
    (heap conc)
    lim in
    let abs' := Build_abstract_state
      (VarMap.upd (avars abs) v site)
      (aheap abs) in
      allCellCompatible conc abs
      -> (forall n1 n2 fl1 fl2, cellCompatible conc' abs' v n1 v n2 fl1 fl2)
      -> (forall v' n n' fl1 fl2, v <> v' -> cellCompatible conc' abs' v n v' n' fl1 fl2)
      -> allCellCompatible conc' abs'.
  unfold allCellCompatible; mySimplify.
  
  destruct (VarMap.sel_upd (vars conc) v (limit conc, false) v1);
    destruct (VarMap.sel_upd (vars conc) v (limit conc, false) v2);
      mySimplify.

  generalize (H v1 n1 v2 n2); unfold cellCompatible; mySimplify.
  assert (followPath v1 n1 fl1 conc r1); eauto.
  assert (followPath v2 n2 fl2 conc r2); eauto.
  destruct (H3 fl1 fl2 r1 r2 H11 H8 H12 H10) as [r1' [r2' [Hr1' [Hr2' Hcell]]]].
  exists r1'; exists r2'; mySimplify.
Qed.

Lemma compatible_write_var_taint : forall conc abs v addr site lim,
  let conc' := Build_state
    (VarMap.upd (vars conc) v addr)
    (heap conc)
    lim in
    let abs' := Build_abstract_state
      (VarMap.upd (avars abs) v site)
      (aheap abs) in
      allTaintCompatible conc abs 
      -> (forall n fl, taintCompatible conc' abs' v n fl)
      -> allTaintCompatible conc' abs'.
  unfold allTaintCompatible; mySimplify.

  destruct (VarMap.sel_upd (vars conc) v (limit conc, false) v0);
    mySimplify.

  generalize (H v0); unfold taintCompatible; mySimplify.
  assert (followPath v0 n fl conc r); eauto.
  destruct (H1 _ _ _ H7 H5) as [r' Hr'].
  trivial.
  mySimplify.
Qed.

Lemma followPath_SO : forall s v n r fl,
  (forall f', cell (NatPairMap.sel (heap s) ((cell (VarMap.sel (vars s) v)), f')) = 0)
  -> followPath v n fl s r
  -> r = VarMap.sel (vars s) v \/ cell r = 0.
  induction 2; mySimplify.
Qed.

Hint Resolve followPath_SO.

Lemma followPath_SO' : forall s v n r fl,
  (forall f', cell (NatPairMap.sel (heap s) ((cell (VarMap.sel (vars s) v)), f')) = 0)
  -> followPath v n fl s r
  -> (n = 0 /\ r = VarMap.sel (vars s) v)
  \/ (n = 1 /\ cell r = 0).
  induction 2; mySimplify.
Qed.

Lemma followPath_S : forall s v n r fl,
  (forall f', cell (NatPairMap.sel (heap s) ((cell (VarMap.sel (vars s) v)), f')) = 0)
  -> followPath v (S n) fl s r
  -> cell r = 0.
  intros.
  generalize (followPath_SO' H H0); mySimplify.
Qed.

Lemma step_Allocate : forall v process conc abs,
  In (Allocate v) (activities process)
    -> compatible conc abs
    -> exists a', In a' (activities (abstractProcess process))
      /\ compatible (exec (Allocate v) conc) (abstract_exec a' abs).
  intros.
  destruct (abstractAllocate _ _ H) as [site Hsite].
  exists (AbsAllocate v site); mySimplify.

  destruct (eq_nat_dec v0 v); subst.

  assert (Hcase : v' = VarMap.sel (vars (Build_state
    (VarMap.upd (vars conc) v (S (limit conc), false)) 
    (heap conc) (S (limit conc)))) v \/ cell v' = 0).
  apply followPath_SO with n fl; mySimplify; rewrite compatZeroed0; auto. 
  mySimplify.

  assert (cell v' <= limit conc); eauto; omega.

  apply compatible_write_var; mySimplify.

  red; intros.
  destruct n1; destruct n2; mySimplify.

  inversion H0; mySimplify.
  exists (site, false).
  exists (site, false).
  inversion H2.
  split; constructor; mySimplify; constructor; mySimplify.

  wrong.
  inversion H0; mySimplify.
  assert (S (limit conc) = 0); try omega.
  remember ({|
       vars := VarMap.upd (vars conc) v (S (limit conc), false);
       heap := heap conc;
       limit := S (limit conc) |}).
  rewrite H3.
  eapply followPath_S with (r:=r2) (s:=s) (v:=v); eauto.
  mySimplify; rewrite compatZeroed0; eauto.

  wrong.
  inversion H2; mySimplify.
  assert (S (limit conc) = 0); try omega.
  remember ({|
       vars := VarMap.upd (vars conc) v (S (limit conc), false);
       heap := heap conc;
       limit := S (limit conc) |}).
  rewrite <- H3.
  eapply followPath_S with (r:=r1) (s:=s) (v:=v); eauto; 
  mySimplify; rewrite compatZeroed0; eauto.

  assert (cell r1 = 0).
  eapply followPath_S; eauto; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.

  red; intros.
  
  assert (Hcases : (n = 0 /\ r1 = VarMap.sel
    (vars (Build_state
      (VarMap.upd (vars conc) v (S (limit conc), false)) 
      (heap conc) (S (limit conc)))) v) \/ (n = 1 /\ cell r1 = 0)).
  apply followPath_SO' with fl1; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.
  wrong.
  assert (S (limit conc) <= limit conc).
  rewrite H4.
  eapply compatInBounds0 with (v':=r2); eauto.
  omega.

  apply compatible_write_var_taint; mySimplify.

  red; intros.
  destruct n; mySimplify.

  inversion H0; subst; mySimplify.

  assert (cell r = 0).
  eapply followPath_S; eauto; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.
Qed.

Lemma step_Source : forall v process conc abs,
  In (Source v) (activities process)
    -> compatible conc abs
    -> exists a', In a' (activities (abstractProcess process))
      /\ compatible (exec (Source v) conc) (abstract_exec a' abs).
  intros.
  destruct (abstractSource _ _ H) as [site Hsite].
  exists (AbsSource v site); mySimplify.

  destruct (eq_nat_dec v0 v); subst.

  assert (Hcase : v' = VarMap.sel (vars (Build_state
    (VarMap.upd (vars conc) v (S (limit conc), true)) 
    (heap conc) (S (limit conc)))) v \/ cell v' = 0).
  apply followPath_SO with n fl; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.

  assert (cell v' <= limit conc); eauto; omega.

  apply compatible_write_var; mySimplify.

  red; intros.
  destruct n1; destruct n2; mySimplify.

  inversion H0; mySimplify.
  exists (site, true).
  exists (site, true).
  inversion H2.
  split; constructor; mySimplify; constructor; mySimplify.

  wrong.
  inversion H0; mySimplify.
  assert (S (limit conc) = 0); try omega.
  remember ({|
       vars := VarMap.upd (vars conc) v (S (limit conc), true);
       heap := heap conc;
       limit := S (limit conc) |}).
  rewrite H3.
  eapply followPath_S with (r:=r2) (s:=s) (v:=v); eauto.
  mySimplify; rewrite compatZeroed0; eauto.

  wrong.
  inversion H2; mySimplify.
  assert (S (limit conc) = 0); try omega.
  remember ({|
       vars := VarMap.upd (vars conc) v (S (limit conc), true);
       heap := heap conc;
       limit := S (limit conc) |}).
  rewrite <- H3.
  eapply followPath_S with (r:=r1) (s:=s) (v:=v); eauto; 
  mySimplify; rewrite compatZeroed0; eauto.

  assert (cell r1 = 0).
  eapply followPath_S; eauto; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.

  red; intros.
  
  assert (Hcases : (n = 0 /\ r1 = VarMap.sel
    (vars (Build_state
      (VarMap.upd (vars conc) v (S (limit conc), true)) 
      (heap conc) (S (limit conc)))) v) \/ (n = 1 /\ cell r1 = 0)).
  apply followPath_SO' with fl1; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.
  wrong.
  assert (S (limit conc) <= limit conc).
  rewrite H4.
  eapply compatInBounds0 with (v':=r2); eauto.
  omega.

  apply compatible_write_var_taint; mySimplify.

  red; intros.

  destruct n; mySimplify.
  exists (site, true).
  split; mySimplify.
  inversion H0.
  apply APath_Done. mySimplify.

  assert (cell r = 0).
  eapply followPath_S; eauto; mySimplify; rewrite compatZeroed0; mySimplify.
  mySimplify.
Qed.

Lemma abstractCopy' : forall src dst process next,
  In (Copy src dst) (activities process)
    -> In (AbsCopy src dst) (activities (abstractProcess' next process)).
  induction process; simplify;
    apply in_app_or in H; firstorder.
Qed.

Lemma abstractCopy : forall src dst process,
  In (Copy src dst) (activities process)
    -> In (AbsCopy src dst) (activities (abstractProcess process)).
  intros; unfold abstractProcess; apply abstractCopy'; trivial.
Qed.

Hint Resolve abstractCopy.

Lemma followPath_swap_var : forall v1 n conc1 r fl,
  followPath v1 n fl conc1 r
  -> forall conc2 v2, VarMap.sel (vars conc1) v1 = VarMap.sel (vars conc2) v2
    -> heap conc2 = heap conc1
    -> followPath v2 n fl conc2 r.
  induction 1; mySimplify.
  rewrite H; trivial.
  rewrite <- H2.
  eauto.
  rewrite <- H2.
  eauto.
Qed.

Hint Resolve AllocSet.incl_In.

Lemma abstract_followPath_incl : forall v1 abs1 abs2 v2 n r fl,
  AllocSet.incl (VarMap.sel (avars abs1) v1) (VarMap.sel (avars abs2) v2) = true
  -> aheap abs1 = aheap abs2
  -> abstract_followPath v1 n fl abs1 r
  -> abstract_followPath v2 n fl abs2 r.
  induction 3; mySimplify.
  apply APath_First_Step with v'; eauto; congruence.
  apply APath_Step with v'; eauto; congruence.
Qed.

Hint Resolve AllocSet.incl_union_right AllocSet.incl_refl.

Lemma step_Copy : forall dst src process conc abs,
  In (Copy dst src) (activities process)
    -> compatible conc abs
    -> exists a', In a' (activities (abstractProcess process))
      /\ compatible (exec (Copy dst src) conc) (abstract_exec a' abs).
  intros.
  exists (AbsCopy dst src); mySimplify.

  destruct (eq_nat_dec v dst); subst.

  apply compatInBounds0 with src n fl.
  
  apply followPath_swap_var with dst
    (Build_state
      (VarMap.upd (vars conc) dst (VarMap.sel (vars conc) src))
      (heap conc) (limit conc)); auto; mySimplify.

  apply compatInBounds0 with v n fl.

  apply followPath_swap_var with v
    (Build_state
      (VarMap.upd (vars conc) dst (VarMap.sel (vars conc) src))
      (heap conc) (limit conc)); auto; mySimplify.

  destruct (eq_nat_dec dst src); subst.

  apply compatible_write_var; auto.

  rewrite AllocSet.union_idempotent.
  repeat rewrite VarMap.upd_self.
  red; intros.
  assert (Hpath1 : followPath src n1 fl1 conc r1).
  eapply followPath_swap_var; eauto; mySimplify.
  assert (Hpath2 : followPath src n2 fl2 conc r2).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H1 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  destruct abs; mySimplify.

  rewrite AllocSet.union_idempotent.
  repeat rewrite VarMap.upd_self.
  red; intros.
  assert (Hpath1 : followPath src n fl1 conc r1).
  eapply followPath_swap_var; eauto; mySimplify.
  assert (Hpath2 : followPath v' n' fl2 conc r2).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H2 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  destruct abs; mySimplify.

  apply compatible_write_var; intuition.

  red; intros.
  assert (Hpath1 : followPath src n1 fl1 conc r1).
  eapply followPath_swap_var; eauto; mySimplify.
  assert (Hpath2 : followPath src n2 fl2 conc r2).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H1 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  eapply (abstract_followPath_incl (v1 := src)); eauto; mySimplify.
  split; auto.
  eapply (abstract_followPath_incl (v1 := src)); eauto; mySimplify.

  red; intros.
  assert (Hpath1 : followPath src n0 fl1 conc r1).
  eapply followPath_swap_var; eauto; mySimplify.
  assert (Hpath2 : followPath v' n' fl2 conc r2).
  eapply followPath_swap_var; eauto; mySimplify.
  unfold allCellCompatible in compatCell0.
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H2 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  eapply (abstract_followPath_incl (v1 := src)); eauto; mySimplify.
  eauto.

  destruct (eq_nat_dec dst src); subst.

  apply compatible_write_var_taint; auto.

  rewrite AllocSet.union_idempotent.
  repeat rewrite VarMap.upd_self.
  red; intros.
  assert (Hpath1 : followPath src n fl conc r).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct (compatTaint0 _ _ _ _ Hpath1 H1) as [r' [Hpath1']].
  mySimplify.
  destruct abs; mySimplify.

  apply compatible_write_var_taint; intuition.

  red; intros.
  assert (Hpath1 : followPath src n0 fl conc r).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct (compatTaint0 _ _ _ _ Hpath1 H1) as [r' [Hpath1']].
  mySimplify.
  exists r'.
  split.
  eapply (abstract_followPath_incl (v1 := src)); eauto; mySimplify.
  mySimplify.
Qed.

Lemma abstractRead' : forall src dst f process next,
  In (Read src dst f) (activities process)
    -> In (AbsRead src dst f) (activities (abstractProcess' next process)).
  induction process; simplify;
    apply in_app_or in H; firstorder.
Qed.

Lemma abstractRead : forall src dst f process,
  In (Read src dst f) (activities process)
    -> In (AbsRead src dst f) (activities (abstractProcess process)).
  intros; unfold abstractProcess; apply abstractRead'; trivial.
Qed.

Hint Resolve abstractRead.

Lemma followPath_read : forall v1 n conc1 r f fl,
  followPath v1 n fl conc1 r
  -> forall conc2, heap conc1 = heap conc2
    -> forall v2, cell (VarMap.sel (vars conc2) v2) <> 0
      -> VarMap.sel (vars conc1) v1
      = NatPairMap.sel (heap conc2) (cell (VarMap.sel (vars conc2) v2), f)
      -> followPath v2 (S n) (fl ++ (f::nil)) conc2 r.
  induction 1; mySimplify.
  rewrite H1; eauto.
  rewrite H1; eauto.
  rewrite H1; eauto.
Qed.

Lemma abstract_followPath_expand : forall v n abs r fl,
  abstract_followPath v n fl abs r
  -> forall abs', aheap abs = aheap abs'
    -> (forall v', AllocSet.incl (VarMap.sel (avars abs) v')
      (VarMap.sel (avars abs') v') = true)
    -> abstract_followPath v n fl abs' r.
  induction 1; intuition.

  eauto.

  apply APath_First_Step with v'.
  eauto.
  congruence.

  apply APath_Step with v'.
  eauto.
  congruence.
Qed.

Lemma incl_fold_union' : forall ls s s' f,
  AllocSet.incl s s' = true
  -> AllocSet.incl s
  (fold_left
    (fun known (site : object) => AllocSet.union known (f site))
    ls
    s') = true.
  induction ls; mySimplify.
  apply IHls.
  apply AllocSet.incl_trans with s'; auto.
  apply AllocSet.incl_union_left.
Qed.

Lemma incl_fold_union : forall ls s f,
  AllocSet.incl s
  (fold_left
    (fun known (site : object) => AllocSet.union known (f site))
    ls
    s) = true.
  intros.
  apply incl_fold_union'; auto.
Qed.

Lemma list_lemma1 : forall l1 l2 (n1 n2:nat),
  l1 ++ n1::nil = n2::l2 -> n1 = hd n1 (rev l2).
  intros.
  destruct l2.
  unfold hd; auto.

  assert(tl (l1 ++ n1 :: nil) = tl (n2 :: n :: l2)).
  rewrite H; auto.

  destruct l1.
  assert (nil ++ n1 :: nil = n1 :: nil); auto.
  rewrite H1 in H; rewrite H1 in H0.
  unfold tl in H0.
  wrong; apply nil_cons in H0; auto.
  assert (((n0 :: l1) ++ n1 :: nil) = n0 :: (l1 ++ n1 :: nil)).
  apply app_comm_cons.
  rewrite H1 in H0.
  unfold tl in H0.
  symmetry in H0.
  rewrite H0.
  destruct l1.
  assert (nil ++ n1 :: nil = n1 :: nil); auto.
  assert (rev ((n3 :: l1) ++ n1 :: nil) = n1 :: rev (n3 :: l1)).
  eapply rev_unit.
  rewrite H2.
  unfold hd.
  auto.
Qed.

Lemma list_lemma1' : forall l1 (n1 n n':nat),
  l1 ++ n1::nil = n::n'::nil -> n1 = n'.
  intros.
  assert (n1 = hd n1 (rev (n'::nil))).
  apply list_lemma1 with l1 n; auto.
  unfold rev in H0.
  assert (nil ++ n' :: nil = n' :: nil); auto.
Qed.

Lemma list_lemma3 : forall (n1 n:nat),
  nil ++ n1::nil = n::nil -> n1 = n.
  intros.
  assert (nil ++ n1 :: nil = n1 :: nil); auto.
  rewrite H0 in H.
  congruence.  
Qed.

Lemma list_lemma3' : forall l (n1 n2 n4:nat),
  n1 :: nil = (n2 :: l) ++ n4 :: nil -> False.
  intros.
  assert (tl (n1 :: nil) = tl ((n2::l) ++ n4::nil)).
  rewrite H; auto.
  assert ((n2 :: l) ++ n4 :: nil = n2 :: (l ++ (n4 :: nil))); auto.
  rewrite H1 in H0.
  unfold tl in H0.
  apply app_cons_not_nil in H0.
  auto.
Qed.

Lemma list_lemma2 : forall l1 l2 (n1 n2:nat),
  l1 ++ n1::nil = n2::l2 -> l1 = rev (tl (rev (n2::l2))).
  intros.
  destruct l2.

  assert (rev (n2 :: nil) = n2 :: nil).
  unfold rev; auto.
  rewrite H0.
  unfold tl.
  apply app_eq_unit in H.
  destruct H.
  destruct H.
  auto.
  destruct H.
  assert (nil <> n1 :: nil).
  apply nil_cons.
  try congruence.

  destruct l1.
  wrong.
  assert (nil ++ n1 :: nil = n1 :: nil); auto.
  rewrite H0 in H.
  congruence.

  symmetry in H.
  rewrite H.
  assert (rev ((n0 :: l1) ++ n1 :: nil) = n1 :: (rev (n0 :: l1))).
  apply rev_unit.
  rewrite H0.
  unfold tl.
  symmetry.
  apply rev_involutive.
Qed.

Lemma list_lemma2' : forall l1 (n1 n n':nat),
  l1 ++ n1::nil = n::n'::nil -> l1 = n::nil.
  intros.
  assert (n::n'::nil = (n::nil) ++ n'::nil); auto.
  assert (n::nil = rev (tl (rev (n::n'::nil)))).
  rewrite H0.  
  assert (rev ((n :: nil) ++ n' :: nil) = n' :: (rev (n :: nil))).
  apply rev_unit.
  rewrite H1.
  unfold tl.
  symmetry.
  apply rev_involutive.
  rewrite H1.
  apply list_lemma2 with n1; auto.
Qed.

Lemma list_lemma4 : forall l1 l2 (n1 n2:nat),
  l1 ++ n1::nil = n2::l2 -> l2 <> nil -> l2 = rev (tl (rev l2)) ++ n1::nil.
  intros.
  assert (n1 = hd n1 (rev l2)).
  apply list_lemma1 with l1 n2; auto.
  assert (rev (rev l2) = l2).
  apply rev_involutive.
  destruct (rev l2).
  unfold rev in H2.
  congruence.
  unfold hd in H1.
  unfold tl.
  subst; auto.
Qed.

Lemma list_lemma5 : forall l1 l2 (n1 n2:nat),
  l1 ++ n1::nil = n2::l2 -> l2 <> nil -> l1 = n2 :: rev (tl (rev l2)).
  intros.
  assert (l1 = rev (tl (rev (n2::l2)))).  
  apply list_lemma2 with n1; auto.
  assert (rev (rev l2) = l2).
  apply rev_involutive.
  assert (rev (n2 :: l2) = rev l2 ++ n2::nil); auto.
  rewrite H3 in H1.
  destruct (rev l2).
  unfold rev in H2.
  congruence.
  rewrite H1.
  assert ((n::l) ++ n2::nil = n:: (l++n2::nil)).
  auto.
  rewrite H4.
  unfold tl.
  apply rev_unit.  
Qed.

Lemma abstract_followPath_read : forall v1 abs1 n' r f fl,
  abstract_followPath v1 n' (fl ++ (f::nil)) abs1 r
  -> forall n, n' = S n
    -> forall abs2, aheap abs1 = aheap abs2
      -> forall v2, (forall a, AllocSet.In a (VarMap.sel (avars abs1) v1) = true
	-> AllocSet.incl (AllocMap.sel (aheap abs1) (cell a, f))
             (VarMap.sel (avars abs2) v2) = true)
          -> abstract_followPath v2 n fl abs2 r.
  induction n'; mySimplify.
  injection H0; mySimplify.
  inversion H; subst.

  destruct fl.
  assert (f = f').
  apply list_lemma3; auto.
  inversion H5; subst; eauto.
  wrong; apply list_lemma3' with fl f' f0 f; auto.

  destruct n0.

  inversion H5; subst.
  assert (f = f'0).
  eapply list_lemma1' with fl f'; auto.
  assert (fl = f'::nil).
  eapply list_lemma2' with f f'0; auto.
  subst.
  eapply APath_First_Step with v'; try congruence; eauto.

  assert (nil <> fl0).
  destruct fl0.
  wrong; inversion H5.
  eapply nil_cons.

  assert (fl0 = rev (tl (rev fl0)) ++ f::nil).
  apply list_lemma4 with fl f'; auto.
  assert (fl = f':: rev (tl (rev fl0))).
  apply list_lemma5 with f; auto.
  rewrite H7.
  eapply APath_Step with v'; try congruence; eauto.
  eapply IHn'; auto.
  symmetry in H4.
  rewrite H4.
  auto.
Qed.

Lemma incl_fold_union_read : forall a f ls s i,
  ls = AllocSet.elems s
  -> AllocSet.In a s = true
  -> AllocSet.incl (f (cell a))
  (fold_left
    (fun known (site : object) =>
      AllocSet.union known (f (cell site)))
    ls
    i)
  = true.
  induction ls; mySimplify;
    generalize (AllocSet.elems_ok s a); rewrite <- H.

  intuition.

  mySimplify.
  apply incl_fold_union'; auto.

  destruct (eq_natboolpair_dec a0 a); subst.
  apply incl_fold_union'; auto.

  apply IHls with (AllocSet.remove s a0).
  symmetry; apply AllocSet.elems_remove; auto.
  apply AllocSet.In_remove; auto.
Qed.

Lemma incl_fold_union_read2 : forall a fr ls s i abs,
  ls = AllocSet.elems s
  -> AllocSet.In a s = true
  -> AllocSet.incl (AllocMap.sel (aheap abs) (cell a, fr))
  (fold_left
    (fun known (site : object) =>
      AllocSet.union known (AllocMap.sel (aheap abs) (cell site, fr)))
    ls
    i)
  = true.
  induction ls; mySimplify;
    generalize (AllocSet.elems_ok s a); rewrite <- H.

  intuition.

  mySimplify.
  apply incl_fold_union'; auto.

  destruct (eq_natboolpair_dec a0 a); subst.
  apply incl_fold_union'; auto.

  apply IHls with (AllocSet.remove s a0).
  symmetry; apply AllocSet.elems_remove; auto.
  apply AllocSet.In_remove; auto.
Qed.

Lemma step_Read : forall dst src f process conc abs,
  In (Read dst src f) (activities process)
    -> compatible conc abs
    -> exists a', In a' (activities (abstractProcess process))
      /\ compatible (exec (Read dst src f) conc) (abstract_exec a' abs).
  intros.
  exists (AbsRead dst src f).

  caseEq (cell (VarMap.sel (vars conc) src)); mySimplify.
  rewrite H1; rewrite compatInitial0; mySimplify.
  rewrite H1; rewrite compatInitial0;
    rewrite H1 in H0; rewrite compatInitial0 in H0;
      mySimplify.
  rewrite H1; rewrite compatInitial0;
    rewrite H1 in H0; rewrite compatInitial0 in H0;
      mySimplify.
  rewrite H1; rewrite compatInitial0;
    mySimplify.

  red; intros.
  red; intros.
  destruct (compatCell0 v1 n1 v2 n2 fl1 fl2 r1 r2) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  apply abstract_followPath_expand with abs; mySimplify;
    VarMap_split; apply incl_fold_union.
  split; auto.
  apply abstract_followPath_expand with abs; mySimplify;
    VarMap_split; apply incl_fold_union.

  rewrite H1; rewrite compatInitial0; mySimplify.

  red; intros.
  red; intros.
  destruct (compatTaint0 v n fl r) as [r' [Hpath1]]; auto.
  exists r'.
  split. apply abstract_followPath_expand with abs; mySimplify;
    VarMap_split; apply incl_fold_union.
  auto.

  destruct (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f));
    destruct n0; mySimplify.

  caseEq (cell (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f))); mySimplify.
  destruct (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f));
    unfold cell in H2; rewrite H2 in H1; mySimplify.

  remember (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f)).
  destruct y.
  destruct (eq_nat_dec dst v); mySimplify.
  apply compatInBounds0 with src (S n0) (fl ++ (f::nil)).
  eapply followPath_read; eauto; mySimplify.

  rewrite H0.
  rewrite H0 in H1.
  destruct (NatPairMap.sel (heap conc) (S n, f)); destruct n0; auto.

  caseEq (cell (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f))); mySimplify.

  red; intros.
  red; intros.
  destruct (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f)).
  unfold cell in H1; subst.
  destruct (compatCell0 v1 n1 v2 n2 fl1 fl2 r1 r2) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  apply abstract_followPath_expand with abs; mySimplify;
    VarMap_split; apply incl_fold_union.
  split; auto.
  apply abstract_followPath_expand with abs; mySimplify;
    VarMap_split; apply incl_fold_union.

  remember (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f)).
  destruct y.
  unfold cell in H1; subst.
  apply compatible_write_var; mySimplify.
  
  red; intros.

  destruct (compatCell0 src (S n1) src (S n2) (fl1 ++ (f::nil)) (fl2 ++ (f::nil)) r1 r2) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  eapply followPath_read; eauto; mySimplify. 
  eapply followPath_read; eauto; mySimplify.
  exists r1'; exists r2'.
  split.
  apply abstract_followPath_read with src abs (S n1) f; mySimplify; eapply incl_fold_union_read2; eauto.
  split; auto.
  apply abstract_followPath_read with src abs (S n2) f; mySimplify; eapply incl_fold_union_read2; eauto.

  red; intros.
  destruct (compatCell0 src (S n1) v' n' (fl1 ++ (f::nil)) fl2 r1 r2) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  eapply followPath_read; eauto; mySimplify.
  eapply followPath_swap_var; eauto; mySimplify.
  exists r1'; exists r2'.
  split.
  apply abstract_followPath_read with src abs (S n1) f;
    mySimplify; eapply incl_fold_union_read2; eauto.
  split; auto.

  caseEq (cell (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f))); mySimplify.

  red; intros.
  red; intros.
  destruct (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f)).
  unfold cell in H1; subst.
  destruct (compatTaint0 v n0 fl r) as [r' [Hpath1]]; auto.
  exists r'.
  split. apply abstract_followPath_expand with abs; mySimplify;
    VarMap_split; apply incl_fold_union.
  auto.

  remember (NatPairMap.sel (heap conc) (cell (VarMap.sel (vars conc) src), f)).
  destruct y.
  unfold cell in H1; subst.
  apply compatible_write_var_taint; mySimplify.
  
  red; intros.
  destruct (compatTaint0 src (S n1) (fl ++ (f::nil)) r) as [r' [Hpath1]]; auto.
  eapply followPath_read; eauto; mySimplify.
  exists r'.
  split.
    apply abstract_followPath_read with src abs (S n1) f;
    mySimplify; eapply incl_fold_union_read2; eauto.
  auto.
Qed.

Lemma abstractWrite' : forall src dst f process next,
  In (Write src f dst) (activities process)
    -> In (AbsWrite src f dst) (activities (abstractProcess' next process)).
  induction process; simplify;
    eapply in_app_or in H; firstorder.
Qed.

Lemma abstractWrite : forall src dst f process,
  In (Write src f dst) (activities process)
    -> In (AbsWrite src f dst) (activities (abstractProcess process)).
  intros; unfold abstractProcess; apply abstractWrite'; trivial.
Qed.

Hint Resolve abstractWrite.

Lemma abstract_followPath_write_preserve : forall v n fl abs1 r,
  abstract_followPath v n fl abs1 r
  -> forall abs2, avars abs1 = avars abs2
    -> (forall addr, AllocSet.incl
      (AllocMap.sel (aheap abs1) addr)
      (AllocMap.sel (aheap abs2) addr) = true)
    -> abstract_followPath v n fl abs2 r.
  induction 1; intuition.

  constructor; congruence.

  eauto.
  eauto.
Qed.

Ltac AllocMap_split :=
  match goal with
    | [ |- context[AllocMap.sel (AllocMap.upd ?M ?A ?V) ?A'] ] =>
      let Haddr := fresh "Haddr" with Heq := fresh "Heq" in
	(destruct (AllocMap.sel_upd M A V A') as [[Haddr Heq] | [Haddr Heq]];
	  rewrite Heq; simplify)
  end.

Hint Resolve AllocSet.incl_union_left.

Lemma incl_write_bonanza' : forall addr h s ls i f,
  (forall addr', AllocSet.incl (AllocMap.sel h addr') (AllocMap.sel i addr') = true)
  -> AllocSet.incl (AllocMap.sel h addr)
  (AllocMap.sel
    (fold_left
      (fun known (site : object) =>
        AllocMap.upd known (cell site, f) (AllocSet.union (AllocMap.sel h (cell site, f)) s))
      ls
      i) addr) = true.
  induction ls; mySimplify.
  apply IHls; mySimplify.
  match goal with
    | [ |- AllocSet.incl ?X ?Y = true ] => generalize (AllocSet.incl_ok X Y)
  end; intuition.
  clear H1.
  apply H2; clear H2; intuition.
  destruct (AllocMap.sel_upd i (cell a, f) (AllocSet.union (AllocMap.sel h (cell a, f)) s) addr'); intuition; subst;
    rewrite H3; eauto.
Qed.

Lemma incl_write_bonanza : forall addr h s ls f,
  AllocSet.incl (AllocMap.sel h addr)
  (AllocMap.sel
    (fold_left
      (fun known (site : object) =>
        AllocMap.upd known (cell site, f) (AllocSet.union (AllocMap.sel h (cell site, f)) s))
      ls
      h) addr) = true.
  intros.
  apply incl_write_bonanza'; auto.
Qed.

Ltac NatPairMap_split :=
  match goal with
    | [ |- context[NatPairMap.sel (NatPairMap.upd ?M ?A ?V) ?A'] ] =>
      let Haddr := fresh "Haddr" with Heq := fresh "Heq" in
	(destruct (NatPairMap.sel_upd M A V A') as [[Haddr Heq] | [Haddr Heq]];
	  rewrite Heq; simplify)
  end.

Lemma fold_left_sum : forall ns n1 n2,
  fold_left (fun x y => S (x + y)) ns (n1 + n2)
  = n1 + fold_left (fun x y => S (x + y)) ns n2.
  induction ns; simplify.
  replace (S (n1 + n2 + a)) with ((n1 + a) + S n2); try omega.
  replace (S (n2 + a)) with (a + S n2); try omega.
  repeat rewrite IHns.
  omega.
Qed.

Lemma followPath_case : forall v n conc' r conc a src fl,
  vars conc' = vars conc
  -> heap conc' = NatPairMap.upd (heap conc) a (VarMap.sel (vars conc) src)
  -> followPath v n fl conc' r
  -> followPath v n fl conc r
  \/ exists n' fl', followPath src n' fl' conc r.
  induction 3; mySimplify.

  rewrite H; eauto.

  rewrite H0; NatPairMap_split.
  rewrite H0; NatPairMap_split.
  destruct H3 as [n [fl Hpath]].
  right.
  inversion Hpath; subst.
  exists 1.
  exists (f'::nil).
  auto.
  exists 2.
  exists (f'::f'0::nil).
  auto.
  exists (S (S (S n0))).
  exists (f'::f'0::fl0).
  auto.

  rewrite H0; NatPairMap_split.

  destruct H3 as [n' [fl' Hpath]].
  rewrite H0; NatPairMap_split.
  right.
  inversion Hpath; subst.
  exists 1.
  exists (f'::nil).
  auto.
  exists 2.
  exists (f'::f'0::nil).
  auto.
  exists (S (S (S n0))).
  exists (f'::f'0::fl0).
  auto.
Qed.

Lemma followPath_nothing_new : forall v n conc' r conc a src fl,
  vars conc' = vars conc
  -> heap conc' = NatPairMap.upd (heap conc) a (VarMap.sel (vars conc) src)
  -> followPath v n fl conc' r
  -> exists v', exists n', exists fl', followPath v' n' fl' conc r.
  intros.
  destruct (followPath_case _ H H0 H1); eauto.
Qed.

Lemma allPaths_step : forall conc' conc src v n r s fl f,
  vars conc' = vars conc
  -> heap conc' = NatPairMap.upd (heap conc) (s,f) (VarMap.sel (vars conc) src)
  -> followPath v n fl conc' r
  -> followPath v n fl conc r
  \/ exists n1, exists n2, exists fl1, exists fl2,
    n = S (n1 + n2)
    /\ fl = fl2 ++ (f::nil) ++ fl1
    /\ (followPath v n1 fl1 conc (s,true) \/ followPath v n1 fl1 conc (s,false)) 
    /\ followPath src n2 fl2 conc' r.

  induction 3; mySimplify.

  rewrite H; eauto.

  destruct (eq_natpair_dec (s,f) (cell v', f')).

  rewrite e in H0.
  assert ((NatPairMap.sel (NatPairMap.upd (heap conc) (cell v', f') (VarMap.sel (vars conc) src)) (cell v', f')) = (VarMap.sel (vars conc) src)).  
  apply NatPairMap.sel_upd_eq; trivial.
  symmetry in H0.
  rewrite H0 in H4.
  rewrite H4.
  right.
  exists 0; exists 0; exists nil; exists nil.
  assert (snd (s, f) = snd (cell v', f')).
  rewrite e; trivial.
  unfold snd in H5.
  rewrite H5.
  split; auto; split; simpl; auto; split.
  destruct v'; destruct b; unfold cell in e.
  assert (fst (s, f) = fst (n, f')).
  rewrite e; trivial.
  unfold fst in H6;
  rewrite H6;
  inversion H1; subst;
  rewrite H; auto.
  assert (fst (s, f) = fst (n, f')).
  rewrite e; trivial.
  unfold fst in H6;
  rewrite H6;
  inversion H1; subst;
  rewrite H; auto.
  symmetry in H.
  rewrite H; auto.

  assert ((NatPairMap.sel (NatPairMap.upd (heap conc) (s, f) (VarMap.sel (vars conc) src)) (cell v', f')) = (NatPairMap.sel (heap conc) (cell v', f'))).
  apply NatPairMap.sel_upd_neq; trivial.
  rewrite H0; rewrite H4; mySimplify.

  destruct (eq_natpair_dec (s,f) (cell v', f')).

  rewrite e in H0.
  assert ((NatPairMap.sel (NatPairMap.upd (heap conc) (cell v', f') (VarMap.sel (vars conc) src)) (cell v', f')) = (VarMap.sel (vars conc) src)).  
  apply NatPairMap.sel_upd_eq; trivial.
  symmetry in H0.
  rewrite H0 in H4.
  rewrite H4.
  right.
  exists 0; exists 0; exists nil; exists nil.
  assert (snd (s, f) = snd (cell v', f')).
  rewrite e; trivial.
  unfold snd in H5.
  rewrite H5.
  split; auto; split; simpl; auto; split.
  destruct v'; destruct b; unfold cell in e.
  assert (fst (s, f) = fst (n, f')).
  rewrite e; trivial.
  unfold fst in H6;
  rewrite H6;
  inversion H1; subst;
  rewrite H; auto.
  assert (fst (s, f) = fst (n, f')).
  rewrite e; trivial.
  unfold fst in H6;
  rewrite H6;
  inversion H1; subst;
  rewrite H; auto.
  symmetry in H.
  rewrite H; auto.

  assert ((NatPairMap.sel (NatPairMap.upd (heap conc) (s, f) (VarMap.sel (vars conc) src)) (cell v', f')) = (NatPairMap.sel (heap conc) (cell v', f'))).
  apply NatPairMap.sel_upd_neq; trivial.
  assert (followPath v 0 nil conc v').
  inversion H1; subst.
  rewrite H; auto.  
  rewrite H0; rewrite H4; mySimplify.

  destruct (eq_natpair_dec (s,f) (cell v', f')).

  rewrite e in H0.
  assert ((NatPairMap.sel (NatPairMap.upd (heap conc) (cell v', f') (VarMap.sel (vars conc) src)) (cell v', f')) = (VarMap.sel (vars conc) src)).  
  apply NatPairMap.sel_upd_eq; trivial.
  symmetry in H0.
  rewrite H0 in H4.
  rewrite H4.
  right.
  exists (S n); exists 0; exists fl; exists nil.
  assert (snd (s, f) = snd (cell v', f')).
  rewrite e; trivial.
  unfold snd in H5.
  rewrite H5.
  split; auto; split; simpl; auto; split.
  destruct v'; destruct b; unfold cell in e.
  assert (fst (s, f) = fst (n0, f')).
  rewrite e; trivial.
  unfold fst in H6;
  rewrite H6;
  inversion H3; subst; auto.
  assert (fst (s, f) = fst (n0, f')).
  rewrite e; trivial.
  unfold fst in H6;
  rewrite H6;
  inversion H3; subst;
  auto.
  symmetry in H.
  rewrite H; auto.

  assert ((NatPairMap.sel (NatPairMap.upd (heap conc) (s, f) (VarMap.sel (vars conc) src)) (cell v', f')) = (NatPairMap.sel (heap conc) (cell v', f'))).
  apply NatPairMap.sel_upd_neq; trivial.
  rewrite H0; rewrite H4.
  mySimplify.

  right.
  destruct H3 as [n1 [n2 [fl1 [fl2 [Hsum [Hfields Hpath]]]]]].
  exists n1; exists (S n2); exists fl1; exists (f'::fl2).
  mySimplify.
  inversion H4; subst; auto.
  inversion H4; subst; auto.
Qed.

Lemma follow_conjoin : forall v1 n1 conc v2 n2 r fl1 fl2,
  followPath v1 n1 fl1 conc (VarMap.sel (vars conc) v2)
  -> followPath v2 n2 fl2 conc r
  -> followPath v1 (n1 + n2) (fl2++fl1) conc r.
  induction 2; mySimplify.

  replace (n1 + 0) with n1; eauto.

  replace (n1 + 1) with (S n1); try omega.
  replace (n1 + 0) with n1 in H2; auto.
  inversion H2; subst; eauto.

  replace (n1 + S( S n)) with (S (S (n1 + n))); try omega; eauto.
  replace (n1 + S n) with (S (n1 + n)) in H2; auto.
Qed.

Lemma abstract_follow_conjoin : forall conc abs abs' v n1 a dst src n2 r' fl1 fl2 f,
  allCellCompatible conc abs
  -> avars abs' = avars abs
  -> (forall addr,
    AllocSet.incl (AllocMap.sel (aheap abs) addr)
    (AllocMap.sel (aheap abs') addr) = true)
  -> (forall r, AllocSet.In r (VarMap.sel (avars abs) dst) = true
    -> AllocSet.incl (VarMap.sel (avars abs) src) (AllocMap.sel (aheap abs') (cell r, f)) = true)
  -> cell a = cell (VarMap.sel (vars conc) dst)
  -> cell a <> 0
  -> followPath v n1 fl1 conc a
  -> abstract_followPath src n2 fl2 abs' r'
  -> abstract_followPath v (S (n1 + n2)) (fl2++f::fl1) abs' r'.

  induction 8; intuition; subst.

  replace (n1 + 0) with n1; try omega.
  replace (nil ++ fl1) with fl1; auto.
  destruct (H v n1 dst 0 fl1 nil a (VarMap.sel (vars conc) dst)) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  inversion Hpath2; subst.
  inversion Hpath1; subst.

  apply APath_First_Step with r1'.
  apply abstract_followPath_write_preserve with abs; auto.
  rewrite Hcell; apply AllocSet.incl_In with (VarMap.sel (avars abs) v0); auto.
  rewrite <- H0; trivial.

  apply APath_Step with r1'.
  apply abstract_followPath_write_preserve with abs; auto.
  rewrite Hcell; apply AllocSet.incl_In with (VarMap.sel (avars abs) v0); auto.
  rewrite <- H0; trivial.

  apply APath_Step with r1'.
  apply abstract_followPath_write_preserve with abs; auto.
  rewrite Hcell; apply AllocSet.incl_In with (VarMap.sel (avars abs) v0); auto.
  rewrite <- H0; trivial.

  replace (n1 + 1) with (S (n1 + 0)); try omega; eauto.
  replace ((f'::nil)++f::fl1) with (f'::(nil++f::fl1)); eauto.

  replace (n1 + S (S n)) with (S (n1 + S n)); try omega; eauto.
  replace ((f'::fl)++f::fl1) with (f'::(fl++f::fl1)); eauto.
Qed.

Lemma allPaths_write : forall conc' conc src abs abs' dst f,
  vars conc' = vars conc
  -> heap conc' = NatPairMap.upd (heap conc) (cell (VarMap.sel (vars conc) dst), f) (VarMap.sel (vars conc) src)
  -> cell (VarMap.sel (vars conc) dst) <> 0
  -> avars abs' = avars abs
  -> (forall addr,
    AllocSet.incl (AllocMap.sel (aheap abs) addr)
    (AllocMap.sel (aheap abs') addr) = true)
  -> (forall r, AllocSet.In r (VarMap.sel (avars abs) dst) = true
    -> AllocSet.incl (VarMap.sel (avars abs) src) (AllocMap.sel (aheap abs') (cell r, f)) = true)
  -> allCellCompatible conc abs
  -> forall n v1 n1 fl1 r1 r2, (cell r1) <> 0
    -> followPath v1 n1 fl1 conc' r1
    -> forall v2 n2 fl2, followPath v2 n2 fl2 conc' r2
      -> cell r1 = cell r2
      -> n1 + n2 <= n
      -> exists r1' r2',
	abstract_followPath v1 n1 fl1 abs' r1'
	/\ abstract_followPath v2 n2 fl2 abs' r2'
        /\ cell r1' = cell r2'.
  induction n; mySimplify.

  destruct n1; mySimplify.
  destruct n2; mySimplify.
  inversion H7; subst.
  inversion H8; subst.
  destruct (H5 v1 0 v2 0 nil nil (VarMap.sel (vars conc') v1) (VarMap.sel (vars conc') v2)) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  rewrite H; auto.
  rewrite H; auto.
  inversion Hpath1; subst.
  inversion Hpath2; subst.
  rewrite <- H2 in H11.
  rewrite <- H2 in H12.
  exists r1'; exists r2'.
  eauto.

  destruct (allPaths_step _ H H0 H7).
  
  destruct (allPaths_step _ H H0 H8).

  destruct (H5 v1 n1 v2 n2 fl1 fl2 r1 r2) as [r1' [r2' [Hpath1 [Hpath2 Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  destruct abs'; mySimplify;
    apply abstract_followPath_write_preserve with abs; auto.
  split; auto.
  destruct abs'; mySimplify;
    apply abstract_followPath_write_preserve with abs; auto.

  destruct H12 as [n1' [n2' [fl1' [fl2' [Hsum [Hpath1 Hpath2]]]]]].
  assert (Hih : exists r1' r2',
    abstract_followPath v1 n1 fl1 abs' r1' /\ abstract_followPath src n2' fl2' abs' r2' /\ cell r1' = cell r2').
  apply IHn with r1 r2; mySimplify.
  destruct Hih as [r1' [r2' [Hapath1 [Hapath2 Hcell]]]].
  exists r1'; exists r2'.
  split; auto.
  split; auto.
  mySimplify.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) = cell ((VarMap.sel (vars conc) dst))); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) <> 0); auto.
  apply (abstract_follow_conjoin H5 H2 H3 H4 H12 H15 H14 Hapath2); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), false) = cell ((VarMap.sel (vars conc) dst))); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) <> 0); auto.
  apply (abstract_follow_conjoin H5 H2 H3 H4 H12 H15 H14 Hapath2); auto.

  destruct H11 as [n1' [n2' [fl1' [fl2' [Hsum [Hpath1 Hpath2]]]]]].
  assert (Hih : exists r1' r2',
    abstract_followPath src n2' fl2' abs' r1' /\ abstract_followPath v2 n2 fl2 abs' r2' /\ cell r1' = cell r2').
  apply IHn with r1 r2; mySimplify.
  destruct Hih as [r1' [r2' [Hapath1 [Hapath2 Hcell]]]].
  exists r1'; exists r2'.
  split; auto.
  mySimplify.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) = cell ((VarMap.sel (vars conc) dst))); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) <> 0); auto.
  apply (abstract_follow_conjoin H5 H2 H3 H4 H11 H14 H13 Hapath1); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), false) = cell ((VarMap.sel (vars conc) dst))); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) <> 0); auto.
  apply (abstract_follow_conjoin H5 H2 H3 H4 H11 H14 H13 Hapath1); auto.
Qed.

Lemma allPaths_write_taint : forall conc' conc src abs abs' dst f,
  vars conc' = vars conc
  -> heap conc' = NatPairMap.upd (heap conc) (cell (VarMap.sel (vars conc) dst), f) (VarMap.sel (vars conc) src)
  -> cell (VarMap.sel (vars conc) dst) <> 0
  -> avars abs' = avars abs
  -> (forall addr,
    AllocSet.incl (AllocMap.sel (aheap abs) addr)
    (AllocMap.sel (aheap abs') addr) = true)
  -> (forall r, AllocSet.In r (VarMap.sel (avars abs) dst) = true
    -> AllocSet.incl (VarMap.sel (avars abs) src) (AllocMap.sel (aheap abs') (cell r, f)) = true)
  -> allCellCompatible conc abs
  -> allTaintCompatible conc abs
  -> forall i n v r fl, cell r <> 0
    -> n <= i
    -> followPath v n fl conc' r
    -> (taint r) = true
    -> exists r',
	abstract_followPath v n fl abs' r'
        /\ (taint r') = true.
  induction i; mySimplify.
  destruct n; mySimplify.
  inversion H9; subst.
  destruct (H6 v 0 nil (VarMap.sel (vars conc') v)) as [r' [Hpath1]]; auto.
  rewrite H; auto.
  inversion Hpath1; subst.
  rewrite <- H2 in H12.
  eauto.

  destruct (allPaths_step _ H H0 H9).

  destruct (H6 v n fl r) as [r' [Hpath1]]; auto.
  exists r'; split;
    destruct abs'; mySimplify;
      apply abstract_followPath_write_preserve with abs; auto.

  destruct H11 as [n1' [n2' [fl1' [fl2' [Hsum [Hpath1 Hpath2]]]]]].
  assert (exists r', abstract_followPath src n2' fl2' abs' r' /\ taint r' = true).
  apply IHi with r; mySimplify.
  destruct H11 as [r' [Hapath1]].
  exists r'.
  mySimplify.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) = cell ((VarMap.sel (vars conc) dst))); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) <> 0); auto.
  apply (abstract_follow_conjoin H5 H2 H3 H4 H12 H15 H14 Hapath1); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), false) = cell ((VarMap.sel (vars conc) dst))); auto.
  assert (cell (cell (VarMap.sel (vars conc) dst), true) <> 0); auto.
  apply (abstract_follow_conjoin H5 H2 H3 H4 H12 H15 H14 Hapath1); auto.
Qed.

Lemma sel_updated' : forall r (f : allocation_site -> AllocSet.set) ls h ff,
  AllocMap.sel h (cell r, ff) = f (cell r)
  -> AllocMap.sel
  (fold_left
    (fun known site => AllocMap.upd known (cell site, ff) (f (cell site))) ls h) (cell r, ff) = f (cell r).
  induction ls; mySimplify.

  rewrite IHls; auto.
  AllocMap_split.
  assert (fst (cell a, ff) = fst (cell r, ff)).
  rewrite Haddr; auto.
  unfold fst in H0; auto.
Qed.

Lemma sel_updated : forall r src f ls dst h ff,
  ls = AllocSet.elems dst
  -> AllocSet.In r dst = true
  -> AllocSet.incl src
  (AllocMap.sel
    (fold_left
      (fun known (site : object) =>
        AllocMap.upd known (cell site, ff)
        (AllocSet.union (f (cell site)) src))
      ls h) (cell r, ff)) = true.
  induction ls; mySimplify. 

  destruct (AllocSet.elems_ok dst r); intuition.
  rewrite <- H in H4.
  inversion H4.

  destruct (eq_natboolpair_dec a r); subst.
  generalize (sel_updated' (r) (fun site => AllocSet.union (f site) src) 
    ls (h := AllocMap.upd h (cell r, ff) (AllocSet.union (f (cell r)) src))); intro.
  unfold AllocMap.dom in H1.
  rewrite H1; auto.
  apply AllocMap.sel_upd_eq.

  apply IHls with (AllocSet.remove dst a).
  symmetry; apply AllocSet.elems_remove; auto.
  apply AllocSet.In_remove; auto.
Qed.

Lemma sel_updated2 : forall r src abs ls dst h ff,
  ls = AllocSet.elems dst
  -> AllocSet.In r dst = true
  -> AllocSet.incl src
  (AllocMap.sel
    (fold_left
      (fun known (site : object) =>
        AllocMap.upd known (cell site, ff)
        (AllocSet.union (AllocMap.sel (aheap abs) (cell site, ff)) src))
      ls h) (cell r, ff)) = true.
  induction ls; mySimplify. 

  destruct (AllocSet.elems_ok dst r); intuition.
  rewrite <- H in H3.
  inversion H3.

  destruct (eq_natboolpair_dec a r); subst.
  generalize (sel_updated' (r) (fun site => AllocSet.union (AllocMap.sel (aheap abs) (site, ff)) src) 
    ls (h := AllocMap.upd h (cell r, ff) (AllocSet.union (AllocMap.sel (aheap abs) (cell r, ff)) src))); intro.
  unfold AllocMap.dom in H1.
  rewrite H1; auto.
  apply AllocMap.sel_upd_eq.

  apply IHls with (AllocSet.remove dst a).
  symmetry; apply AllocSet.elems_remove; auto.
  apply AllocSet.In_remove; auto.
Qed.


Lemma step_Write : forall dst src f process conc abs,
  In (Write dst f src) (activities process)
    -> compatible conc abs
    -> exists a', In a' (activities (abstractProcess process))
      /\ compatible (exec (Write dst f src) conc) (abstract_exec a' abs).
  intros.
  exists (AbsWrite dst f src).

  caseEq (cell (VarMap.sel (vars conc) dst)); mySimplify.

  rewrite H1; auto.
  rewrite H1; rewrite H1 in H0; eauto.
  rewrite H1; rewrite H1 in H0; auto.
  rewrite H1.

  red; intros.
  red; intros.
  destruct (compatCell0 v1 n1 v2 n2 fl1 fl2 r1 r2) as [r1' [r2' [Hvar [Hheap Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  apply abstract_followPath_write_preserve with abs; simpl; intuition;
    apply incl_write_bonanza.
  split; auto.
  apply abstract_followPath_write_preserve with abs; simpl; intuition;
    apply incl_write_bonanza.

  rewrite H1.

  red; intros.
  red; intros.
  destruct (compatTaint0 v n fl r) as [r' [Hvar Hheap]]; auto.
  exists r'.
  split.
  apply abstract_followPath_write_preserve with abs; simpl; intuition;
    apply incl_write_bonanza.
  mySimplify.

  rewrite H0; simpl.
  rewrite NatPairMap.sel_upd_neq; auto.
  mySimplify.

  rewrite H0; rewrite H0 in H1.
  simpl.

  assert (exists v'', exists n'', exists fl'', followPath v'' n'' fl'' conc v').

  apply followPath_nothing_new with v n0
    (Build_state (vars conc)
      (NatPairMap.upd (heap conc) (S n, f) (VarMap.sel (vars conc) src))
      (limit conc))
    (S n, f) src fl; auto.
  destruct H2 as [v'' [n'' [fl'' Hn'']]]; eauto.

  rewrite H0; rewrite H0 in H1; mySimplify.
  rewrite NatPairMap.sel_upd_neq; auto.
  assert (S n <= limit conc).
  rewrite <- H0.
  apply compatInBounds0 with dst 0 nil; trivial.
  rewrite <- H0; trivial.
  assert (fst (cell (VarMap.sel (vars conc) dst), f) <> fst (a, f0)).
  unfold fst; omega.
  intuition; eauto.
  rewrite H4 in H3; auto.

  rewrite H0.
  red; intros.
  red; intros.
  apply allPaths_write with
    (Build_state (vars conc)
      (NatPairMap.upd (heap conc) (S n, f) (VarMap.sel (vars conc) src))
      (limit conc))
    conc src abs dst f (n1 + n2) r1 r2; simpl; intuition.
  rewrite H0; trivial.

  apply incl_write_bonanza.
  eapply sel_updated2; eauto.

  rewrite H0.
  red; intros.
  red; intros.
  apply allPaths_write_taint with
    (Build_state (vars conc)
      (NatPairMap.upd (heap conc) (S n, f) (VarMap.sel (vars conc) src))
      (limit conc))
    conc src abs dst f n0 r; simpl; intuition.
  rewrite H0; trivial.

  apply incl_write_bonanza.
  eapply sel_updated2; eauto.
Qed.

Lemma abstractSanitize' : forall src dst process next,
  In (Sanitize dst src) (activities process)
    -> In (AbsSanitize dst src) (activities (abstractProcess' next process)).
  induction process; simplify;
    apply in_app_or in H; firstorder.
Qed.

Lemma abstractSanitize : forall src dst process,
  In (Sanitize dst src) (activities process)
    -> In (AbsSanitize dst src) (activities (abstractProcess process)).
  intros; unfold abstractProcess; apply abstractSanitize'; trivial.
Qed.

Hint Resolve abstractSanitize.

Lemma followPath_sanitize_var1 : forall v1 n conc1 r fl,
  followPath v1 (S n) fl conc1 r
  -> forall conc2 v2, VarMap.sel (vars conc1) v1 = (cell (VarMap.sel (vars conc2) v2), false)
    -> heap conc2 = heap conc1
    -> cell (VarMap.sel (vars conc2) v2) <> 0
    -> followPath v2 (S n) fl conc2 r.
  induction n; mySimplify.

  inversion H; subst.
  assert (followPath v2 0 nil conc2 (VarMap.sel (vars conc2) v2)); auto.
  apply Path_First_Step with (f':=f') in H5; auto.
  inversion H3; subst.
  assert (cell (VarMap.sel (vars conc2) v2) = cell (VarMap.sel (vars conc1) v1)).
  destruct (VarMap.sel (vars conc1) v1).
  destruct (VarMap.sel (vars conc2) v2).
  unfold cell in H0.
  assert (cell (n, b) = cell (n0, false)); rewrite H0; auto.
  rewrite <- H6; rewrite <- H1; auto.

  inversion H; subst.
  generalize (IHn conc1 v' fl0 H4 conc2 v2 H0 H1 H2); intros.
  rewrite <- H1.
  eauto.
Qed.

Lemma followPath_sanitize_var2 : forall v1 conc1 r,
  followPath v1 0 nil conc1 (r, false)
  -> forall conc2 v2, VarMap.sel (vars conc1) v1 = (cell (VarMap.sel (vars conc2) v2), false)
    -> heap conc2 = heap conc1
  -> exists b, followPath v2 0 nil conc2 (r, b).
  intros.
  inversion H; subst.
  assert (followPath v2 0 nil conc2 (VarMap.sel (vars conc2) v2)); auto.
  destruct (VarMap.sel (vars conc2) v2).
  exists b; auto.
  rewrite H0 in H3.
  unfold cell in H3.
  assert (cell (n, false) = cell (r,false)).
  rewrite H3; auto.
  unfold cell in H4; subst; auto.
Qed.

Lemma incl_sanitize_fold1 : forall (f : object -> AllocSet.set) x ls s,
  AllocSet.In x (fold_left
    (fun known site =>
      AllocSet.union known (f site))
    ls s) = true
  -> (AllocSet.In x s = true
    \/ exists y, In y ls /\ AllocSet.In x (f y) = true).
  induction ls; simpl; intuition.

  generalize (IHls (AllocSet.union s (f a))); intuition.
  destruct (AllocSet.In_union H0); intuition.
  right; eauto.

  destruct H0; intuition.
  right; eauto.
Qed.

Lemma incl_sanitize_fold2 : forall (f : object -> AllocSet.set) x ls s,
  (AllocSet.In x s = true
    \/ exists y, In y ls /\ AllocSet.In x (f y) = true)
  -> AllocSet.In x (fold_left
    (fun known site =>
      AllocSet.union known (f site))
    ls s) = true.
  induction ls; simpl; intuition.

  firstorder.

  generalize (IHls (AllocSet.union s (f a))); intuition eauto.

  generalize (IHls (AllocSet.union s (f a))); intuition.
  destruct H0; intuition; subst.
  eauto.
  apply H2.
  eauto.
Qed.

Lemma incl_fold_sanitize : forall (f : object -> AllocSet.set) s1 s2 t,
  AllocSet.incl s1 s2 = true
  -> AllocSet.incl
  (fold_left
    (fun known site =>
      AllocSet.union known (f site))
    (AllocSet.elems t) s1)
  (fold_left
    (fun known site =>
      AllocSet.union known (f site))
    (AllocSet.elems t) s2) = true.
  intros.
  apply AllocSet.incl_ok.
  mySimplify.
  apply incl_sanitize_fold1 in H0.
  apply incl_sanitize_fold2.
  destruct H0.
  left; eauto.
  right; eauto.
Qed.

Lemma abstract_followPath_sanitize_incl2 : forall v1 abs1 abs2 v2 n r fl,
  AllocSet.incl
    (fold_left (fun known site => AllocSet.union known (AllocSet.add AllocSet.empty ((cell site), false)))
      (AllocSet.elems (VarMap.sel (avars abs1) v1)) AllocSet.empty) (VarMap.sel (avars abs2) v2) = true
  -> aheap abs1 = aheap abs2
  -> abstract_followPath v1 (S n) fl abs1 r
  -> abstract_followPath v2 (S n) fl abs2 r.
  induction n; mySimplify.

  inversion H1; subst.
  inversion H3; subst.
  assert (AllocSet.In (cell v', false) (VarMap.sel (avars abs2) v2) = true).
  eapply (AllocSet.incl_ok).
  apply H.
  generalize (AllocSet.elems_ok (VarMap.sel (avars abs1) v1) v'); intuition.
  apply incl_sanitize_fold2.
  right.
  exists v'; auto.
  mySimplify.
  inversion H2; subst; auto.
  apply APath_First_Step with (cell v', false); auto.
  rewrite <- H0; unfold cell; trivial. 

  inversion H1; subst.
  generalize (IHn v' fl0 H H0 H3); intros.
  eapply APath_Step with v'; auto.
  rewrite <- H0; auto.
Qed.

Lemma step_Sanitize : forall dst src process conc abs,
  In (Sanitize dst src) (activities process)
    -> compatible conc abs
    -> exists a', In a' (activities (abstractProcess process))
      /\ compatible (exec (Sanitize dst src) conc) (abstract_exec a' abs).
  intros.
  exists (AbsSanitize dst src); mySimplify.

  destruct (eq_nat_dec v dst); subst.

  destruct n.
  inversion H0; mySimplify.

  destruct (eq_nat_dec (cell (VarMap.sel (vars conc) src)) 0).

  rewrite e in H0.

  assert (cell v' = 0).
  apply followPath_S with (Build_state (VarMap.upd (vars conc) dst (0, false))
      (heap conc) (limit conc)) dst n fl; mySimplify.
  generalize (compatInitial0 f').
  intros.
  destruct (NatPairMap.sel (heap conc) (0, f')).
  assert (cell (n0, b) = cell (0, false)); rewrite H1; auto.
  omega.

  apply compatInBounds0 with src (S n) fl.
  apply followPath_sanitize_var1 with dst
    (Build_state (VarMap.upd (vars conc) dst (cell (VarMap.sel (vars conc) src), false))
      (heap conc) (limit conc)); auto; mySimplify.

  apply compatInBounds0 with v n fl.
  apply followPath_write_var with dst
    (Build_state (VarMap.upd (vars conc) dst (cell (VarMap.sel (vars conc) src), false))
      (heap conc) (limit conc)) (cell (VarMap.sel (vars conc) src), false) (limit conc); auto.

  apply compatible_write_var; intuition.

  red; intros.

  destruct n1; destruct n2.
  inversion H0; inversion H2.

  assert (Hpath1 :exists b1, followPath src 0 nil conc (cell r1, b1)).
  inversion H0; mySimplify.
  eapply followPath_sanitize_var2; eauto; mySimplify.
  assert (Hpath2 :exists b2, followPath src 0 nil conc (cell r2, b2)).
  inversion H2; mySimplify.
  destruct Hpath1 as [b1 Hpath1]; destruct Hpath2 as [b2 Hpath2].
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H1 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists (cell r1', false); exists (cell r2', false).
  split.
  apply APath_Done.
  mySimplify.
  inversion Hpath1'; subst.
  generalize (AllocSet.elems_ok (VarMap.sel (avars abs) src) r1'); intuition.
  apply incl_sanitize_fold2.
  right.
  exists r1'; auto.
  split; auto.
  apply APath_Done.
  mySimplify.
  inversion Hpath2'; subst.
  generalize (AllocSet.elems_ok (VarMap.sel (avars abs) src) r2'); intuition.
  apply incl_sanitize_fold2.
  right.
  exists r2'; auto.

  destruct (eq_nat_dec (cell (VarMap.sel (vars conc) src)) 0).

  inversion H0; mySimplify.

  inversion H0.

  assert (Hpath1 :exists b1, followPath src 0 nil conc (cell r1, b1)).
  inversion H0; mySimplify.
  eapply followPath_sanitize_var2; eauto; mySimplify.
  assert (Hpath2 : followPath src (S n2) fl2 conc r2).
  eapply followPath_sanitize_var1; eauto; mySimplify.
  destruct Hpath1 as [b1 Hpath1].
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H1 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists (cell r1', false); exists r2'.
  split.
  apply APath_Done.
  mySimplify.
  inversion Hpath1'; subst.
  generalize (AllocSet.elems_ok (VarMap.sel (avars abs) src) r1'); intuition.
  apply incl_sanitize_fold2.
  right.
  exists r1'; auto.
  split; auto.
  eapply (abstract_followPath_sanitize_incl2 (v1 := src) (abs1:=abs)); eauto; mySimplify.
  eapply incl_fold_sanitize.
  apply AllocSet.incl_empty.

  destruct (eq_nat_dec (cell (VarMap.sel (vars conc) src)) 0).

  inversion H2; mySimplify.

  inversion H2.

  assert (Hpath2 :exists b2, followPath src 0 nil conc (cell r2, b2)).
  inversion H2; mySimplify.
  eapply followPath_sanitize_var2; eauto; mySimplify.
  assert (Hpath1 : followPath src (S n1) fl1 conc r1).
  eapply followPath_sanitize_var1; eauto; mySimplify.
  destruct Hpath2 as [b2 Hpath2].
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H1 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists r1'; exists (cell r2', false).
  split.
  eapply (abstract_followPath_sanitize_incl2 (v1 := src) (abs1:=abs)); eauto; mySimplify.
  eapply incl_fold_sanitize.
  apply AllocSet.incl_empty.
  split; auto.
  apply APath_Done.
  mySimplify.
  inversion Hpath2'; subst.
  generalize (AllocSet.elems_ok (VarMap.sel (avars abs) src) r2'); intuition.
  apply incl_sanitize_fold2.
  right.
  exists r2'; auto.

  destruct (eq_nat_dec (cell (VarMap.sel (vars conc) src)) 0).

  rewrite e in H0.
  assert (cell r1 = 0).
  apply followPath_S with (Build_state (VarMap.upd (vars conc) dst (0, false))
      (heap conc) (limit conc)) dst n1 fl1; mySimplify.
  generalize (compatInitial0 f').
  intros.
  destruct (NatPairMap.sel (heap conc) (0, f')).
  assert (cell (n, b) = cell (0, false)); rewrite H4; auto.
  congruence.

  assert (Hpath1 : followPath src (S n1) fl1 conc r1).
  eapply followPath_sanitize_var1; eauto; mySimplify.
  assert (Hpath2 : followPath src (S n2) fl2 conc r2).
  eapply followPath_sanitize_var1; eauto; mySimplify.
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H1 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  eapply (abstract_followPath_sanitize_incl2 (v1 := src) (abs1:=abs)); eauto; mySimplify.
  eapply incl_fold_sanitize.
  apply AllocSet.incl_empty.
  split; auto.
  eapply (abstract_followPath_sanitize_incl2 (v1 := src) (abs1:=abs)); eauto; mySimplify.
  eapply incl_fold_sanitize.
  apply AllocSet.incl_empty.

  red; intros.

  destruct n.

  inversion H1.

  assert (Hpath1 :exists b1, followPath src 0 nil conc (cell r1, b1)).
  inversion H1; mySimplify.
  eapply followPath_sanitize_var2; eauto; mySimplify.
  assert (Hpath2 : followPath v' n' fl2 conc r2).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct Hpath1 as [b1 Hpath1].
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H2 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists (cell r1', false); exists r2'.
  split.
  apply APath_Done.
  mySimplify.
  inversion Hpath1'; subst.
  generalize (AllocSet.elems_ok (VarMap.sel (avars abs) src) r1'); intuition.
  apply incl_sanitize_fold2.
  right.
  exists r1'; auto.
  split; auto.

  destruct (eq_nat_dec (cell (VarMap.sel (vars conc) src)) 0).

  rewrite e in H1.
  assert (cell r1 = 0).
  apply followPath_S with (Build_state (VarMap.upd (vars conc) dst (0, false))
      (heap conc) (limit conc)) dst n fl1; mySimplify.
  generalize (compatInitial0 f').
  intros.
  destruct (NatPairMap.sel (heap conc) (0, f')).
  assert (cell (n0, b) = cell (0, false)); rewrite H5; auto.
  congruence.

  assert (Hpath1 : followPath src (S n) fl1 conc r1).
  eapply followPath_sanitize_var1; eauto; mySimplify.
  assert (Hpath2 : followPath v' n' fl2 conc r2).
  eapply followPath_swap_var; eauto; mySimplify.
  destruct (compatCell0 _ _ _ _ _ _ _ _ Hpath1 H2 Hpath2) as [r1' [r2' [Hpath1' [Hpath2' Hcell]]]]; auto.
  exists r1'; exists r2'.
  split.
  eapply (abstract_followPath_sanitize_incl2 (v1 := src) (abs1:=abs)); eauto; mySimplify.
  eapply incl_fold_sanitize.
  apply AllocSet.incl_empty.
  split; auto.

  apply compatible_write_var_taint; intuition.

  red; intros.

  destruct n.

  wrong.
  inversion H0; subst.
  mySimplify.

  destruct (eq_nat_dec (cell (VarMap.sel (vars conc) src)) 0).

  rewrite e in H0.
  assert (cell r = 0).
  apply followPath_S with (Build_state (VarMap.upd (vars conc) dst (0, false))
      (heap conc) (limit conc)) dst n fl; mySimplify.
  generalize (compatInitial0 f').
  intros.
  destruct (NatPairMap.sel (heap conc) (0, f')).
  assert (cell (n0, b) = cell (0, false)); rewrite H3; auto.
  congruence.

  assert (Hpath : followPath src (S n) fl conc r).
  eapply followPath_sanitize_var1; eauto; mySimplify.
  destruct (compatTaint0 _ _ _ _ Hpath H1 H2) as [r' [Hpath' Htaint']]; auto.
  exists r'; split; auto.
  eapply (abstract_followPath_sanitize_incl2 (v1 := src) (abs1:=abs)); eauto; mySimplify.
  eapply incl_fold_sanitize.
  apply AllocSet.incl_empty.
Qed.

Section allocation_site_model_is_conservative.
  Variable process : compound_activity activity.

  Lemma allocation_site_step :
    forall conc abs, compatible conc abs
      -> forall process a, In a (activities process)
	-> compatible (exec a conc) abs
	\/ exists a', In a' (activities (abstractProcess process))
	  /\ compatible (exec a conc) (abstract_exec a' abs).
    destruct a; intuition.
    
    right; apply step_Allocate; intuition.
    right; apply step_Source; intuition.
    right; apply step_Copy; intuition.
    right; apply step_Sanitize; intuition.
    right; apply step_Read; intuition.
    right; apply step_Write; intuition.
  Qed.

  Theorem allocation_site_model_is_conservative :
    forall conc conc', reachable_flowInsensitive exec process conc conc'
      -> forall abs, compatible conc abs
	-> exists abs', reachable_flowInsensitive abstract_exec (abstractProcess process)
	  abs abs'
	  /\ compatible conc' abs'.
    induction 1; intuition.

    exists abs; intuition.
    subst.

    assert (compatible (exec a s1) abs
      \/ exists a', In a' (activities (abstractProcess process))
	/\ compatible (exec a s1) (abstract_exec a' abs)).
    apply allocation_site_step; intuition.
    destruct H0 as [Hcompat | [ins' [Hin Hcompat]]]; auto.

    destruct (IHreachable_flowInsensitive _ Hcompat) as [abs' [Hreach Hcompat']].
    eauto.
  Qed.

  Definition abstract_initState :=
    Build_abstract_state
    (VarMap.init AllocSet.empty)
    (AllocMap.init AllocSet.empty).

  Lemma andersen_start :
    forall conc, reachable_flowInsensitive exec process initState conc
      -> exists abs, reachable_flowInsensitive abstract_exec (abstractProcess process)
	abstract_initState abs
	  /\ compatible conc abs.
    intros.
    eapply allocation_site_model_is_conservative; eauto.

    intuition.

    unfold initState; simpl.
   apply NatPairMap.sel_init.

    assert (forall s, followPath v n fl s v'
      -> s = initState
      -> (cell v') <= limit initState); eauto.
    induction 1; mySimplify.
    rewrite VarMap.sel_init; auto.

    unfold initState; simpl.
    apply NatPairMap.sel_init.

    red; intros; red; intros.
    wrong.
    assert (forall s, followPath v1 n1 fl1 s r1
      -> s = initState
      -> False); eauto.
    induction 1; mySimplify.

    assert ((VarMap.sel (VarMap.init (0, false)) v) = (0, false)).
    apply VarMap.sel_init.
    apply H1.
    assert (cell (VarMap.sel (VarMap.init (0, false)) v) = 0).
    rewrite H4; unfold cell; trivial.
    auto.
    assert ((NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = (0, false)).
    apply NatPairMap.sel_init.
    apply H1.
    assert (cell (NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = 0).
    rewrite H6; unfold cell; trivial.
    auto.
    assert ((NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = (0, false)).
    apply NatPairMap.sel_init.
    apply H1.
    assert (cell (NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = 0).
    rewrite H6; unfold cell; trivial.
    auto.

    unfold allTaintCompatible, initState, taintCompatible.
    mySimplify.
    inversion H0; mySimplify.

    wrong.
    assert ((VarMap.sel (VarMap.init (0, false)) v) = (0, false)).
    apply VarMap.sel_init.
    apply H1.
    assert (cell (VarMap.sel (VarMap.init (0, false)) v) = 0).
    rewrite H3; unfold cell; trivial.
    auto.
    wrong.
    assert ((NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = (0, false)).
    apply NatPairMap.sel_init.
    apply H1.
    assert (cell (NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = 0).
    rewrite H5; unfold cell; trivial.
    auto.
    wrong.
    assert ((NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = (0, false)).
    apply NatPairMap.sel_init.
    apply H1.
    assert (cell (NatPairMap.sel (NatPairMap.init (0, false)) (cell v', f')) = 0).
    rewrite H5; unfold cell; trivial.
    auto.
  Qed.

  Theorem andersen_sound : forall v,
    (forall abs, reachable_flowInsensitive abstract_exec (abstractProcess process)
      abstract_initState abs
      -> forall site, AllocSet.In site (VarMap.sel (avars abs) v) = true
         -> taint site = false)
    -> mustNotBeTainted process v.
   
    unfold mustNotBeTainted.
    intros.
    generalize (flowInsensitive_is_conservative H0); intro Hfi.
    destruct (andersen_start Hfi) as [abs [Hexec Hcompat]].
    generalize (H _ Hexec).
    intros.
    intuition.
    remember (VarMap.sel (vars s) v) as site.
    generalize (compatTaint0 v 0 nil site).
    intros.

    remember (taint site).
    destruct (taint site); auto.

    destruct H3; mySimplify.
    generalize (H2 x).
    inversion H4; mySimplify.
    congruence.
  Qed.

End allocation_site_model_is_conservative.
