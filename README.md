# Coq sources for *Certified Information Flow Analysis of Service Implementations*

[![build status](https://gitlab.com/t.heinze/soca2018/badges/master/pipeline.svg)](https://gitlab.com/t.heinze/soca2018/commits/master)

To run the analysis for the paper's example (assuming Ubuntu Linux):
```
$ apt-get update
$ apt-get install opam -y
$ opam init -n --comp=4.01.0 -j 2
$ opam repo add coq-released http://coq.inria.fr/opam/released
$ opam install -y coq.8.4pl4 && opam pin add coq 8.4pl4
$ eval `opam config env`
$ make Andersen.ml
$ cat Process.ml >> Andersen.ml
$ ocaml Andersen.ml
```

Coq sources are based upon [prior work by Adam Chlipala](http://adam.chlipala.net/itp/coq/src/)

## Reference

 * [Thomas S. Heinze, Jasmin Türker: Certified Information Flow Analysis of Service Implementations. SOCA 2018](https://doi.org/10.1109/SOCA.2018.00033)
